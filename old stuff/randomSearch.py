import random

import numpy
import matplotlib.pyplot as plt



CAPACITY = 100
NUM_ITEMS = 10


#dictionary of tuples key = item tuple = (weight, value)
#items = {'a':(1,1),'b':(1,1),'c':(1,1),'d':(1,1),'e':(1,1),'f':(1,1),'g':(1,1),'h':(1,1),'i':(1,1),'j':(1,1)}

#better as a list of tuples (weight,value)
#items = [(10,10),(20,25),(15,10),(5,15),(50,40),(30,35),(15,20),(25,30),(40,20),(10,5)]

items = [(10,10),(20,25),(15,10),(5,15),(50,40),(30,40),(15,20),(25,30),(40,40),(10,5)]



def evalKnapsack(individual):
    weight = 0
    value = 0
    
    for i in range(NUM_ITEMS):
        if individual[i]==1:        
            weight += items[i][0]
            value += items[i][1]
    
    if weight > CAPACITY:
        #return 10000, 0 #terrible fitness
        return 0,
     
    #return weight, value
    return value,
    
def genNewInd(length):
    ind=[]
    for i in range(length):
        ind.append(random.randint(0,1))
    #ind = [random.randint(0,1)]*NUM_ITEMS    
    print (ind)
    return ind
    
def randomSearch(ngen):
    bestFitness=0
    for gen in range(ngen):
        
        if gen == 0:
            best = genNewInd(NUM_ITEMS)
            bestFitness = evalKnapsack(best)
        else:
            new = genNewInd(NUM_ITEMS)
            newFitness = evalKnapsack(new)
            if newFitness>bestFitness:
                bestFitness=newFitness
                best=new
        print("Best = ",best)
        print("Fitness = ",bestFitness)
    
    
    
    return best, bestFitness
    
def main():
    best,fitness = randomSearch(100)
    
    
   

                 
if __name__ == "__main__":
    random.seed(1)
    #for i in range(30):
     #   pop, log, hof = main()
      #  print("Best individual is: %s\nwith fitness: %s" % (hof[0], hof[0].fitness))
        #print (pop)
    #main() 
    best, fitness = randomSearch(100)