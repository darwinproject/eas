LENGTHS=(256 784 529 144 400 16 25 289 36 676 169 49 441 576 64 324 196 81 729 225 100 484 361 625 121)

for l in ${LENGTHS[@]}
do
	echo $l
	(for seed in {0..180..20}
	do
		echo "    $seed"
		echo $l > ~/IsingSummary/IsingSpinGlass_pm_${l}_$seed.txt
		Release/P3 config/default.cfg -runs 1 -problem IsingSpinGlass -length $l -problem_seed $seed -max_evals 10000 -verbosity 2 >> ~/IsingSummary/IsingSpinGlass_pm_${l}_$seed.txt
	done) 2> ~/IsingSummary/IsingSpinGlass_${l}_summary.txt
done
