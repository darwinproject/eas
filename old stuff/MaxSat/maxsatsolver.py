# -*- coding: utf-8 -*-
'''
Created on Fri Mar 10 14:18:38 2017

@author: Michael
'''

import random

import numpy as np

from statsmodels import robust

import os
from numpy.random import choice
from deap import base
from deap import creator
from deap import tools
import math

precision = 65536
fastGA = False
MU = 20
LAMBDA = 20
crossover = False
max_evals = 10000
BETA = 1.5
F = 1.5
c = (1 / LAMBDA)
lambdalambda = False
adjust = True

creator.create('FitnessMin', base.Fitness, weights=(1.0, ))
creator.create('Individual', list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register('attr_bool', random.randint, 0, 1)
toolbox.register('mate', tools.cxUniform, indpb=0.5)
toolbox.register('mutate', tools.mutFlipBit, indpb=0.1)
toolbox.register('select', tools.selBest)
toolbox.register('mateBest', tools.cxUniform, indpb=c)


def selParents(individuals, k, toolbox):
    parents = [random.choice(individuals) for i in range(k)]
    return [toolbox.clone(ind) for ind in parents]


def binomialMutate(p):
    l = np.random.binomial(length, p)
    return l


def llOffSpring(population, toolbox, LAMBDA):
        offspring = []  # Copy old generators
        l = binomialMutate(LAMBDA / length)
        for i in range(round(LAMBDA)):
            offspring.append(toolbox.clone(population[0]))
            toolbox.mutate(offspring[i], indpb=l/length)
        fitnesses = toolbox.map(toolbox.evaluate, offspring)
        for ind, fit in zip(offspring, fitnesses):
            ind.fitness.values = fit

        bestOne = toolbox.select(offspring, 1)[0]
        for j in range(round(LAMBDA)):
            bestOneClone = toolbox.clone(bestOne)
            parentClone = toolbox.clone(population[0])
            toolbox.mateBest(parentClone, bestOneClone)
            offspring[j] = parentClone
            del offspring[j].fitness.values
        return offspring


def read(filename):
    with open(filename) as f:
        solution = [int(x) for x in f.readline().strip()]
        clauses = []
        signs = []
        for line in f:
            clause = []
            sign = []
            for pair in line.strip().split(' '):
                s, v = [int(x) for x in pair.split(',')]
                clause.append(v)
                sign.append(s)
            clauses.append(clause)
            signs.append(sign)
        return solution, signs, clauses


def float_round(value, precision):
    return round(value * precision) / precision


def powerDistribution(n):
    prob = 0
    for i in range(1, round(n/2)):
        prob += (math.pow(i, -BETA))
    return prob


def fmut():
    CB = powerDistribution(length)
    alphas = list(range(1, int(length/2)))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB, -1)*math.pow(alphas[i], -BETA))
    draw = choice(alphas, 1, p=probs)
    return draw


def evalMaxSat(individual):
    total = 0
    for i in range(len(clauses)):
        for c in range(3):
            if (individual[clauses[i][c]] == signs[i][c]):
                total += 1
                break
    return float_round(float(total) / len(clauses), precision),


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(fitness_count < max_evals):
        if(fastGA):
            alpha = fmut()
            toolbox.register('mutate', tools.mutFlipBit, indpb=alpha/length)
        gen += 1
        offspring = [toolbox.clone(ind) for ind in population]
        parent = toolbox.clone(population[0])
        if lambdalambda:
            offspring = llOffSpring(population, toolbox, LAMBDA)
        else:
            if crossover:
                for i in range(1, len(offspring), 2):
                    offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                    del offspring[i - 1].fitness.values, offspring[i].fitness.values

            for i in range(len(offspring)):
                offspring[i], = toolbox.mutate(offspring[i])
                del offspring[i].fitness.values

        #  Evaluate the individuals with an invalid fitness
        offspring = toolbox.select(offspring, LAMBDA)
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fitness_count += len(invalid_ind)

        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        #  Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)
        else:
            if(adjust):
                if(population[0].fitness.values <= parent.fitness.values):
                    LAMBDA = min(N, (LAMBDA * (math.pow(F, 1/4))))
                else:
                    LAMBDA = max(1, (LAMBDA / F))
                k = LAMBDA
                c = (1 / k)
                p = k / N
                toolbox.register('mateBest', tools.cxUniform, indpb=c)
                toolbox.register('mutate', tools.mutFlipBit, indpb=p)

        population = toolbox.select(population+offspring, MU)

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
#        print(logbook.stream)
#        if max(logbook.select('max')) == 1.0:
#            break

    return population, logbook


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register('avg', np.mean)
    stats.register('std', np.std)
    stats.register('min', np.min)
    stats.register('max', np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover,
                     stats, halloffame=hof)

    return pop, log, hof

if __name__ == '__main__':
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    toolbox.register('evaluate', evalMaxSat)
    toolbox.register("selectParents", selParents)

    results_file = ('results/' + str(MU) + '+' + str(LAMBDA) +
                    ('Fast' if fastGA else '') +
                    ('GA' if crossover else 'EA') + '/')
    if lambdalambda:
        results_file = ('results/' + str(MU) + '+' + 'lambda' + ',' +
                        'lambda' + ('GA' if crossover else 'EA') + '/')

    if not os.path.exists(results_file):
        os.makedirs(results_file)

    for i, f in enumerate(sorted(os.listdir('test_data'))[:1]):
        global length
        global signs
        global solution
        global clauses
        global optimum

        length = int(f.split('_')[1])
        solution, signs, clauses = read('test_data/'+f)
        toolbox.register('individual', tools.initRepeat, creator.Individual,
                         toolbox.attr_bool, length)
        #  Structure initializers
        toolbox.register('population', tools.initRepeat, list,
                         toolbox.individual)

        pop, log, hof = main()
        # Set the output file here

        with open(results_file+f, 'w') as l:
                print(log, file=l)
        evals.append(sum(log.select('nevals')))
        if (evalMaxSat(hof[0])[0] != 1.0):
            failures += 1

        print('Run:', i,
              'Evals:', evals[-1],
              'MES:', np.median(evals),
              'MAD:', robust.mad(evals),
              'FAILURES:', failures,
              'Best:', hof[0].fitness.values[0],
              'Solution:', evalMaxSat(solution)[0])

        if counter % 10 == 9:
            problem_size = length
            # Set the output file here as well
            with open(results_file + 'MaxSat-summary-' + str(problem_size) +
                      '.txt', 'w') as s:
                print(summary, file=s)
            summary = ''
        summary += ('Run:' + str(i) +
                    ' Evals:' + str(evals[-1]) +
                    ' MES:' + str(np.median(evals)) +
                    ' MAD:' + str(robust.mad(evals)) +
                    ' FAILURES:' + str(failures) +
                    ' Best:' + str(hof[0].fitness.values[0]) +
                    ' Solution:' + str(evalMaxSat(solution))[0] + '\n')
        counter += 1
    with open(results_file + 'MaxSat-summary-' + str(length) + '.txt',
              'w') as s:
        print(summary, file=s)
