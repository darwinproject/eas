# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 10:36:47 2017

@author: Michael
"""

import os
import pandas as pd
import numpy as np
import re
data = pd.DataFrame()
ising_columns = ['Size', 'Run', 'Evals', 'MES', 'MAD', 'Failures', 'Average Best']
mn = {1: (5, 100), 2: (5, 250), 3: (10, 100), 4: (5, 500), 5: (10, 250),
      6: (10, 500), 7: (30, 100), 8: (30, 250), 9: (30, 500)}
mkp_columns = ['M', 'N', "Alpha", 'Run', 'Evals', 'MES', 'MAD',
               'Failures', 'Best', 'Solution']


def process_ising(results_folder):
    rows = []
    for f in [f for f in os.listdir(results_folder) if "summary" in f]:
      
        
        try:
            de= re.findall('\d+', f)[0]
            size = int(de)
            #print(de)
          
        except:
            print(results_folder+f)
            continue
   
        with open(results_folder+os.sep+f) as summary:
            
               # print("Yooo",f)
            for line in summary:
                if line.strip() == '':
                    continue
                details = line.split()
                row = [size]
                for d in details:
                    k, v = d.split(":")
                    if k == "Run":
                        v = int(v) % 10
                        
                    row.append(float(v))
             
                  
                rows.append(row)
                   
    data = pd.DataFrame(data=rows,
                        columns=ising_columns+['Solution'])
    data = data.drop('Solution', 1)

    averages = pd.DataFrame(columns=ising_columns)
 
    for i in range(0, data.Failures.count(), 10):
        mean = pd.DataFrame(columns=ising_columns, data=[data[i:i+10].mean().values])
        mean["NOPT"] = pd.DataFrame(columns=["NOPT"], data=[sum([i==1 for i in data[i:i+10]['Average Best'].values])])
        averages = pd.concat([averages, mean])
        
    return averages.sort_values(['Size'])


def process_mkp(results_folder):
    rows = []
    for f in [f for f in os.listdir(results_folder) if "summary" in f]:
        m, n = mn[int(f.split("-")[0][-1])]
        alpha = 0.25
        count = 0
        with open(results_folder+os.sep+f) as summary:
            for line in summary:
                if line.strip() == '':
                    continue
                details = line.split()
                row = [m, n, alpha]
                for d in details:
                    k, v = d.split(":")
                    if k == "Run":
                        v = int(v) % 10
                    row.append(float(v))
                rows.append(row)
                count += 1
                if count == 10:
                    alpha = 0.5
                if count == 20:
                    alpha = 0.75
    data = pd.DataFrame(data=rows, columns=mkp_columns)
    data['Frac'] = data['Best']/data['Solution']

    averages = pd.DataFrame(columns=mkp_columns+["Average Best"])

    for i in range(0, data.M.count(), 10):
        mean = pd.DataFrame(columns=mkp_columns+["Average Best"], data=[data[i:i+10].mean().values])
        mean["NOPT"] = pd.DataFrame(columns=["NOPT"], data=[sum([i>=1 for i in data[i:i+10]['Frac'].values])])
        averages = pd.concat([averages, mean])

    return averages.sort_values(['Alpha', 'M', 'N'])


with open("results/tables/presentation.tex", 'w') as tex:
    print("\\documentclass{article}\n", file=tex)
    print("\\usepackage{booktabs}\n", file=tex)
    print("\\begin{document}", file=tex)
    print("\t\\section{Ising Spin Glass}", file=tex)
    print("ISING")
    for file_name in os.listdir("Ising/results/"):
        save_folder = 'results/tables/Ising/'
        averages = process_ising("Ising/results/"+file_name)
      
        try:
            with open(save_folder+file_name+".tex", 'w') as t:
                averages.to_latex(index=False, buf=t, columns=['Size', 'Average Best', 'Evals', 'NOPT'])
                print("Saving to", save_folder+file_name)
            print("\t\\begin{table}[ht!]", file=tex)
            print("\t\t\\centering", file=tex)
            print("\t\t\\input{Ising/"+file_name+".tex}", file=tex)
            print("\t\t\\caption{The performance of the "+file_name+"}", file=tex)
            print("\t\t\\label{table:ising"+file_name+"}", file=tex)
            print("\t\\end{table}", file=tex)
            print("\t\\newpage\n", file=tex)
        except:
            print("FAILED TO SAVE", file_name)

    print("\t\\section{Multidimensional Knapsack Problem}", file=tex)
    print("MKP")
    for file_name in os.listdir("MKP/results/"):
        save_folder = 'results/tables/MKP/'
        averages = process_mkp("MKP/results/"+file_name)
        try:
            with open(save_folder+file_name+".tex", 'w') as t:
                averages.to_latex(columns=["M", "N", "Alpha", "Average Best", "NOPT"],
                              index=False,
                              buf=t)
                print("Saving to", save_folder+file_name)
            print("\t\\begin{table}[ht!]", file=tex)
            print("\t\t\\centering", file=tex)
            print("\t\t\\input{MKP/"+file_name+".tex}", file=tex)
            print("\t\t\\caption{The performance of the "+file_name+"}", file=tex)
            print("\t\t\\label{table:mkp"+file_name+"}", file=tex)
            print("\t\\end{table}", file=tex)
            print("\t\\newpage\n", file=tex)
        except:
            print("FAILED TO SAVE", file_name)

    print("\\end{document}", file=tex)



