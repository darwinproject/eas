# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 17:05:06 2017

@authors: JP, James
"""

import random
import numpy as np
import copy
import math
import sys
import ast
from os import listdir
from statsmodels import robust
from deap import base
from deap import creator
from deap import tools

# Attempt to describe mgs problem
# Basically knapsack problem but:
# Elements S are knapsacks with capacities equal to their total values C(s_i)=s_i
# Elements T are objects with weights equal to their respective values w(t_i)=t_i
# Every knapsack needs to be filled

MU = 50
LAMBDA = 1
crossoverMode = "mean"
ALPHA = 0.25
max_evals = 100000
numParents = 4

creator.create("FitnessMin", base.Fitness, weights=(-1.0, ))
# Define Individuals and Chromosomes
creator.create("Individual", list, fitness=creator.FitnessMin)
creator.create("Chromosome", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register("select", tools.selBest)
toolbox.register("selectParents", tools.selRandom)

def random_size(n, a, b):
    randoms = []
    for i in range(n):
        random.seed()
        s = random.randint(a, b)
        randoms.append(s)
    return randoms


def delta(w, fi):
    if w <= fi:
        return w
    else:
        return 0


# Perform random greedy for population initialisation, and at the end of
# crossover (if necessary)
# Container is to make the lists "Individuals"
def rg_mgs(container, nSample, objs):
    # generated objects
    o = []
    # free spaces
    f = objs[:]
    # knapsacks which contain each object
    obj_sets = []
    # Add a new list of lists to show which knapsacks hold each new object
    #k.append([[] for i in range(len(objs))])
    # Do until generating set is created
    while (max(f) != 0):
        # Randomly sample n objects between 1 and maximum free space
        r = random_size(nSample, 1, max(f))
        # Generate an object and update appropriate lists/sacks
        f, o, obj_sets, = generate(f, o, obj_sets, r)
    # Eliminate duplicate objects
    knaps = elim_duplicates(o, obj_sets)
    return container(knaps)


# Generate a new best object based on given free space
def generate(free_spaces, objects, object_sacks, sampled_objects):
    # Get a list of each object's global contribution (how good it is)
    psilist = psi(sampled_objects, free_spaces)
    # Get best one
    argmax = max(psilist)
    om_index = psilist.index(argmax)
    # Add best object to list of objects
    omega_prime = sampled_objects[om_index]
    # print("best global object", omega_prime)
    objects.append(omega_prime)
    contained_by = []
    # Check all knapsacks and if they can hold this new object, add it
    # Update all list relating to which knapsacks contain this object
    for i in range(len(free_spaces)):
        if(omega_prime <= free_spaces[i]):
            free_spaces[i] -= omega_prime
            contained_by.append(i)
    object_sacks.append(contained_by)
    return free_spaces, objects, object_sacks


# Get global contribution of a list of objects, related to how much free space
# is left in each knapsack
def psi(R, F):
    psilist = []
    for w in R:
        contrib = 0
        for fi in F:
            contrib += delta(w, fi)
    psilist.append(contrib)
    return psilist


# Eliminate duplicate objects
def elim_duplicates(o, k):
    o_prime = copy.deepcopy(o)
    duplicates = True
    while(duplicates):  # If there are no duplicates
        if (duplicates):
            if(check_indices(o_prime)):
                # Get the i and j locations of the duplicate objects within a
                # knapsack
                i, j = check_indices(o_prime)
                # Create new object 2*weight, add to objects
                if (intersect(k[i], k[j]) != []):
                    new_obj = 2*o_prime[i]
                    o_prime.append(new_obj)
                    knaps_cont_new_obj = intersect(k[i], k[j])
                    k.append(knaps_cont_new_obj)
                k[i] = complement(union(k[i], k[j]), intersect(k[i], k[j]))
                # Check if original object is only ever used in duplicates
                # If only ever duplicates, delete both original objects, else
                # delete only 1
                if(k[i] == []):
                    del o_prime[i]
                    del k[i]
                del o_prime[j]
                del k[j]
        # Check for more duplicates
        duplicates = check(o_prime)
    # Combine the objects with the knapsacks which hold them as a tuple, for
    # ease in seeing what is going on
    final = []
    for obj, knaps in zip(o_prime, k):
        final.append((obj, knaps))
    return final


# Set functions
def intersect(a, b):
    return list(set(a) & set(b))


def union(a, b):
    return list(set(a) | set(b))


def complement(a, b):
    return list(set(a) - set(b))


# Check for duplicate objects
def check(o):
    checker = []
    checker_bool = False
    for i in range(len(o)):
        for j in range(len(o)):
            if (i != j and i < j):
                if(o[i] == o[j]):
                    # # # # # JAMES SAYS RETURN TRUE# # # # #
                    checker.append(True)
                else:
                    checker.append(False)
    if True in checker:
        checker_bool = True
    return checker_bool


# Find the i and j indices for a duplicate object
def check_indices(o):
    for i in range(len(o)):
        for j in range(len(o)):
            if (i != j and i > j and o[i] == o[j]):
                return i, j
            else:
                continue
    return False


# Evaluate a fitness, make sure there's a comma afterwards, otherwise dea
# errors
def eval_fitness(k):
    return len(k),
toolbox.register("evaluate", eval_fitness)

# Oh god why, crossover time boi
def crossover_operator(mode, parents, k, csample, ns, alpha, container):
    # New list Oparent
    oparent = []
    parentCopies = []
    limit = 1
    parents = sorted(parents, key = len, reverse = True)
    for p in parents:
        parentCopies.append(copy.deepcopy(p))
        if(len(p)<limit or limit ==1):
            limit = len(p)
    f = k[:]
    count = 0
    # New offspring
    offspring = []
    while(count < limit):
        # Get objects, and delete from parents so can't be reselected
        parentObjs = [0 for i in range(len(parents))]
        
        
        for i in range(len(parentCopies)):
            if(i==0):
                parentObjs[0], p1objKnap = max(parentCopies[0], key=lambda x: x[0])
            else:
                parentObjs[i] = most_similar(parentObjs, parentCopies[i])
            parentCopies[i] = [tup for tup in parentCopies[i] if (tup[0] != parentObjs[i])]
        
        minim = 0
        maxim = 0
        # Check which is bigger for JP's hacky random generator
        for ob in range(len(parentObjs)):
            if(parentObjs[ob]<minim or ob==0):
                minim = parentObjs[ob]
            if(parentObjs[ob]>maxim or ob==0):  
                 maxim = parentObjs[ob]

        if(mode == "mean"):
            minim, maxim = pr_mean_range(alpha, [minim, maxim], max(f))
            if(minim <= maxim):
                mean_sampled_objects = np.random.randint(minim, maxim+1, csample)
            f, oparent, offspring = generate(f, oparent, offspring, mean_sampled_objects)
        else:
            if(mode == "parent"):
                minim, maxim = pr_parent_range(alpha, [minim, maxim], parentObjs[0], max(f))
                if(minim <= maxim):
                    parent_sampled_objects = np.random.randint(minim, maxim+1, csample)
                f, oparent, offspring = generate(f, oparent, offspring, parent_sampled_objects)
            # JP's hacky random generator
            else:
                sampled_objects = random_size(csample, minim, maxim)
                f, oparent, offspring = generate(f, oparent, offspring, sampled_objects)
        count+=1
    fr = f[:]
    new_objs = [[] for i in range(len(fr))]
    # If there's still free space in offspring after above, use rg_mgs to fill
    while(max(f) != 0):
        r = random_size(ns, 1, max(f))
        f, oparent, new_objs = generate(f, oparent, offspring, r)
    # Eliminate duplicates in offspring and return
    offspring = elim_duplicates(oparent, offspring)
    return container(offspring)

# Get the most similar object in the second parent
def most_similar(o, p):
    lowestDist = 0
    retobj = o[-1]
    count = 0
    for x, knaplle in p:
        currDist=0
        for obj in o:
            currDist += abs(x-obj)
        if (count==0):
            lowestDist = currDist
        if(currDist<lowestDist):
                lowestDist = currDist
                retobj = x
    return retobj

def pr_mean_range(a, p, fmax):
    x = p[0]
    y = p[1]
    i = y - x
    mean = (x+y)/2
    mini = int(max([1, mean-(i*(1+a))]))
    maxi = int(min([fmax, mean+(i*(1+a))]))
    return mini, maxi

def pr_parent_range(a, p, w, fmax):
    x = p[0]
    y = p[1]
    i = y-x
    mini = int(max([1, w-(i*a)]))
    maxi = int(min([fmax, w+(i*a)]))
    return mini, maxi

def getxy(p):
    mini = 100000
    maxi = 0
    for parent in p:
        for weight, knap in parent:
            mini = weight if(weight<mini) else mini
            maxi = weight if(weight > maxi) else maxi
    return mini, maxi



# Generate a random list of objects to use as a set
def generate_objects(n, lowest, highest):
    return (random_size(n, lowest, highest))


# upper = 2147483647
upper = 50
def generate_unconstrained(n):
    return random_size(n, 1, upper)

def generate_limited(n, m):
    s = random_size(n, 1, upper)
    print("generated s", s)
    smax = max(s)
    tmax = int((2*smax)/m)
    print("smax:", smax, "tmx:", tmax)
    candidate_solution = random_size(m, 1, tmax)
    print("candidate", candidate_solution)
    news = []
    for num in s:
        subset = random.sample(range(1, m), 1)
        count = 0
        news_val = 0
        while(count<subset):
            tind = random.randint(0, m-1)
            if(news_val+candidate_solution[tind] <= smax):
                news_val += candidate_solution[tind]
            count+=1
        news.append(news_val)
    return news


# Do the GA part
def ga_mgs(k, MU, LAMBDA, nsample, csample , objs, alpha, cmode, toolbox, ngen, stats=None, halloffame=None, verbose=__debug__):
    # optimal upper bound of solution
    # O = math.log(max(objs), 2)
    # Do logbook things for printing and storing
    gen=0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Set pop size to NP
    pop = toolbox.population(n=MU)
    # Go through the individuals and set their fitnesses
    eval_count = 0
    for ind in pop:
        for obj, chrom in ind:
            chrom = creator.Chromosome(chrom)
        ind.fitness.values = toolbox.evaluate(ind)
        eval_count+=1
     # initialise hall of fame
    if halloffame is not None:
        halloffame.update(pop)

    # Do logbook
    record = stats.compile(pop) if stats else {}
    logbook.record(gen=gen, nevals=eval_count, **record)

    # Begin the generational process
    while(eval_count<max_evals):
        gen+=1
        offspring = []
        # select 2 parents using binary tournament selection
        for off in range(LAMBDA):
            parents = toolbox.selectParents(pop, numParents)
            # apply crossover to get one offspring
            offspring.append(crossover_operator(cmode, parents, objs, csample, nsample, alpha, creator.Individual))
            # Eval fitness of offspring
            offspring[off].fitness.values = toolbox.evaluate(offspring[off])
            eval_count+=1
        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)
        # Get the next generation's population based on current + offspring
        pop = toolbox.select(pop+offspring, MU)
        # Append the current generation statistics to the logbook
        record = stats.compile(pop) if stats else {}
        logbook.record(gen=gen, nevals=LAMBDA, **record)

        if halloffame[0].fitness.values[0] <= int(O):
            break
        fits = [len(x) for x in pop]
        same = len(set(fits))==1
        if same==True:
            ci = tools.selRandom(pop, 1)
            pop = toolbox.population(n=MU-1)
            pop = toolbox.select(pop+ci, MU)
            ec = eval_count
            for ind in pop:
                for obj, chrom in ind:
                    chrom = creator.Chromosome(chrom)
                ind.fitness.values = toolbox.evaluate(ind)
                eval_count+=1
             # initialise hall of fame
            if halloffame is not None:
                halloffame.update(pop)
            record = stats.compile(pop) if stats else {}
            logbook.record(gen=gen, nevals=eval_count-ec, **record)
            
        
    return pop, logbook, halloffame


def read(file):
    smax = 0
    opt = 0
    nlist = []
    probs = []
    with open(file, 'r') as f:
        smax = int(f.readline().strip())
        opt = ast.literal_eval(f.readline().strip())
        for line in f:
            if ("[" in line):
                newp = ast.literal_eval(line.strip())
                probs.append(newp)
            else:
                if "0" in line:
                    newn = int(line)
                    nlist.append(newn)
        return smax, opt, nlist, probs



def main():
    # n = 50
    # smax = 65536
    # problem = generate_objects(n, 1, smax)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    
    pop, log, hof = ga_mgs([], MU, LAMBDA, nsample, csample, problem, ALPHA, crossoverMode, toolbox, max_evals, stats, halloffame=hof)

    return pop, log, hof
    
def main2():
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop = toolbox.population(n=10000)
    for ind in pop:
        for obj, chrom in ind:
            chrom = creator.Chromosome(chrom)
        ind.fitness.values = toolbox.evaluate(ind)
    hof.update(pop)
    return hof

#knapsacks = []
#problem=[333, 465, 86, 768, 324, 63, 876, 897, 326, 854]
#O = int(math.log(897,2))
#print(int(math.log(897,2)))
#csample = 10
#nsample = 5
#toolbox.register("individual", rg_mgs, creator.Individual, knapsacks, nsample, problem)
#toolbox.register("population", tools.initRepeat, list, toolbox.individual)
#toolbox.register("selectParents", tools.selRandom)
#pop, log, hof = main()
#print(log[-1])
#print(hof[0])
#print(hof[0].fitness.values[0])

if __name__ == "__main__":
    print("starting")
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0
    knapsacks=[]
    nsample=5 # sample of random numbers
    csample = 10
    results_file = 'results/'
    savefile = 'Errorfile'
    for f in sorted(listdir('test_data')):
        print("curr file",f)
        global smax        
        global O
        global Nplist
        global problemlist
        global problem
        smax, O, Nplist, problemlist = read('test_data/'+f)
        if(len(Nplist)==len(problemlist)):
            run = 0
            for i in range(len(problemlist)):
                if crossoverMode=="popOnly":
                    problem = problemlist[i]
                    savefile = 'MGS_Smax-'+str(smax)+'_n-'+str(Nplist[run])+'_averages-pop10000'
                    results_file = 'results/averages/'
                    for t in range(50):
                        toolbox.register("individual", rg_mgs, creator.Individual, nsample, problem)
                        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
                        hof = main2()
                        
                        summary+= ("Best:" + str(hof[0].fitness.values[0]) +
                                " Solution:" + str(O) + "\n")
                    break;
                    
                else:
                    savefile = 'MGS_Smax-'+str(smax)+'_n-'+str(Nplist[run])
                    problem = problemlist[i]
                    toolbox.register("individual", rg_mgs, creator.Individual, nsample, problem)
                    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
                    pop, log, hof = main()
                    evals.append(sum(log.select("nevals")))
                    results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+'GA/'
                    with open(results_file+savefile+'.txt', 'w') as l:
                            print(log, file=l)
                    if (hof[0].fitness.values[0] > int(O)):
                        failures += 1
            
                    print("Run:", run,
                          "Evals:", evals[-1],
                          "MES:", np.median(evals),
                          "MAD:", robust.mad(evals),
                          "FAILURES:", failures,
                          "Best:", hof[0].fitness.values[0],
                          "Solution:", O)
            
                    summary += ("Run:" + str(run) +
                                " Evals:" + str(evals[-1]) +
                                " MES:" + str(np.median(evals)) +
                                " MAD:" + str(robust.mad(evals)) +
                                " FAILURES:" + str(failures) +
                                " Best:" + str(hof[0].fitness.values[0]) +
                                " Solution:" + str(O) + "\n")
                    print(hof[0])
                    run+=1
        else:
            print("Error: N list inequal length to problem list")
        with open(results_file+savefile+'-summary.txt', 'w') as s:
                print(summary, file=s)
        summary = ''
    
    print("finished")