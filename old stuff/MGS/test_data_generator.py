# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 15:51:51 2017

@author: James
"""
import math
import random

def generate_objects(n,lowest,highest):
    return (random_size(n,lowest,highest))

def random_size(n,a,b):
    randoms=[]
    for i in range(n):
        random.seed()
        s=random.randint(a,b)
        randoms.append(s)
    return randoms
    
for smax in [1000]:
    test_data = ''
    optimum_upper = int(math.log(smax,2))
    test_file = 'test_data/'
    test_data += (str(smax) + "\n" +
    str(optimum_upper)+ "\n")
    generated_problem = generate_objects(10,1,smax)
    test_data += (str(10) + "\n" +
                str(generated_problem)+ "\n")
    with open(test_file+'MGS_test_data_Smax-'+str(smax)+'.txt', 'w') as s:
        print(test_data, file=s)

#for smax in [65536,1048576,16777216]:
#    test_data = ''
#    optimum_upper = int(math.log(smax,2))
#    test_file = 'test_data/'
#    test_data += (str(smax) + "\n" +
#        str(optimum_upper)+ "\n")
#    for n in range(50,1000,50):
#        generated_problem = generate_objects(n,1,smax)
#        test_data += (str(n) + "\n" +
#                str(generated_problem)+ "\n")
#    for n in range(1000,5001,1000):
#        generated_problem = generate_objects(n,1,smax)
#        test_data += (str(n) + "\n" +
#                str(generated_problem)+ "\n")
#    with open(test_file+'MGS_test_data_Smax-'+str(smax)+'.txt', 'w') as s:
#        print(test_data, file=s)
    
print("done")

#random.seed(1000)
#    evals = []
#    failures = 0
#    summary = ''
#    counter = 0
#    knapsacks=[]
#    global problem
#    # problem=[333, 465, 86, 768, 324, 63, 876, 897, 326, 854]
#    nsample=10 # sample of random numbers
#    csample = 6
#    for smax in [65536,1048576,16777216]:
#        O = int(math.log(smax,2))
#        run = 0
#        for Np in range(50,1000,50):
#            print("current problem set size", Np)
#            savefile = 'MGS_Smax-'+str(smax)+'_n-'+str(Np)
#            problem = generate_objects(Np,1,smax)
#            toolbox.register("individual", rg_mgs, creator.Individual, knapsacks, nsample, problem)
#            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
#            pop = toolbox.population(n=Np)
#            pop, log, hof = main()
#            evals.append(sum(log.select("nevals")))
#            results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+'GA/'
#            with open(results_file+savefile+'.txt', 'w') as l:
#                    print(log, file=l)
#            if (hof[0].fitness.values[0] > int(O)):
#                failures += 1
#    
#            print("Run:", run,
#                  "Evals:", evals[-1],
#                  "MES:", np.median(evals),
#                  "MAD:", robust.mad(evals),
#                  "FAILURES:", failures,
#                  "Best:", hof[0].fitness.values[0],
#                  "Solution:", O)
#    
#            summary += ("Run:" + str(run) +
#                        " Evals:" + str(evals[-1]) +
#                        " MES:" + str(np.median(evals)) +
#                        " MAD:" + str(robust.mad(evals)) +
#                        " FAILURES:" + str(failures) +
#                        " Best:" + str(hof[0].fitness.values[0]) +
#                        " Solution:" + str(O) + "\n")
#            run+=1
#        for Np in range(1000,10001,1000):
#            print("current problem set size", Np)   
#            savefile = 'MGS_Smax-'+str(smax)+'_n-'+str(Np)
#            problem = generate_objects(Np,1,smax)
#            toolbox.register("individual", rg_mgs, creator.Individual, knapsacks, nsample, problem)
#            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
#            pop = toolbox.population(n=Np)
#            pop, log, hof = main()
#            evals.append(sum(log.select("nevals")))
#            results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+'GA/'
#            with open(results_file+savefile+'.txt', 'w') as l:
#                    print(log, file=l)
#    
#            if (hof[0].fitness.values[0] > int(O)):
#                failures += 1
#    
#            print("Run:", run,
#                  "Evals:", evals[-1],
#                  "MES:", np.median(evals),
#                  "MAD:", robust.mad(evals),
#                  "FAILURES:", failures,
#                  "Best:", hof[0].fitness.values[0],
#                  "Solution:", O)
#    
#            summary += ("Run:" + str(run) +
#                        " Evals:" + str(evals[-1]) +
#                        " MES:" + str(np.median(evals)) +
#                        " MAD:" + str(robust.mad(evals)) +
#                        " FAILURES:" + str(failures) +
#                        " Best:" + str(hof[0].fitness.values[0]) +
#                        " Solution:" + str(O) + "\n")
#            run+=1
#        with open(results_file+savefile+'-summary.txt', 'w') as s:
#                print(summary, file=s)