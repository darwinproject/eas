# Michael's messing about copy of knapsack.py
import random
import or_reader
import numpy as np
import os

from statsmodels import robust

from deap import base
from deap import creator
from deap import tools
from numpy.random import choice
import math

import time

def powerDistribution(n, BETA):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob
    
def fmut(N, BETA):
    CB=powerDistribution(N, BETA)
    alphas = list(range(1, int(N/2)))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
    draw=choice(alphas,1,p=probs)
    return draw

def valid(individual, capacities, coefficients):
    constraints = np.dot(coefficients, individual)
    return all([i < 0 for i in(constraints - capacities)])

def generateIndividual(container, size):
    # Start with an empty bag
    ind = [0] * size
    # Add items to the bag until it's not valid
    while valid(ind):
        index = random.randint(0, size-1)
        ind[index] = 1
    # Remove the last item so we've got a valid bag again
    ind[index] = 0
    return container(ind)

# select parents uniformly at random with replacement
def selParents(individuals, k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))
    return parents

def evalKnapsack(individual, values):
    if valid(individual):
        return np.dot(individual, values),
    return 0,