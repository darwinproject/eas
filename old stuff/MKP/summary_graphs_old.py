# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 23:34:25 2017

@author: James
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 14:01:39 2017

@author: Michael
#Summed up nevals so we could use them for the graph instead of gens - James
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
test1="1+8,8GA"
test2="1+10EA"
test3="100+1EA"
test4="20+20GA"
test5="1+1EA"
test6="CB100NoRepair"
test7="10+1EA"
test9="1+lambda,lambdaGA"
tests=[test9,test5]
#colors=["#089FFF","#FF000F","#00FF22","#124452"]

   
color=iter(cm.rainbow(np.linspace(0,1,len(tests))))

for i in range(len(tests)):
    
    c=next(color)
    results_folder = "mkp/results/"+tests[i]+"/"
    averages = pd.DataFrame(columns=['gen', 'nevals', 'avg', 'std', 'min', 'max'])
    n=3
    for filename in [f for f in os.listdir(results_folder) if "summary" not in f][:29]:
        print(filename)
        with open(results_folder+filename) as f:
            headings = [s.strip() for s in f.readline().split("\t")]
            rows = []
            for r in f:
                row = [s.strip() for s in r.split("\t")]
                if len(row) == len(headings):
                    rows.append([float(r) for r in row])
        
            table = pd.DataFrame(rows, columns=headings)
            j=0
            for index, row in table.iterrows():
                    j+=row['nevals']
                    row['nevals']=j
            averages = pd.concat([averages, table]) 
  
    #print(averages)

    table = averages.groupby(level=0).mean()
  
    plt.rcParams["font.family"] = "CMU Serif"
    plt.title("Comparison of Different Algorithms on MKP")
    plt.xlabel("Fitness Functions")
    plt.ylabel("Fitness")
    n=1

    plt.plot(table['nevals'], table['avg'], '-', c=c,  linewidth=2, alpha=0.7, label="Average: "+tests[i])
# 
#    plt.fill_between(table['nevals'], table['min'], table['max'],
#                     alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
#                     label="Min max")
#    plt.fill_between(table ['nevals'], table['avg']-table['std'],
#                     table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
#                     antialiased=True, linewidth=0, label="Standard deviation")
    plt.legend(loc=0)

plt.show()