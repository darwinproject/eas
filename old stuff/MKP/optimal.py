# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 14:27:38 2017

@author: Michael
"""

import os

for folder in os.listdir("results/max"):
    for summary in [f for f in os.listdir("results/max/"+folder) if "summary" in f]:
        with open("results/max/"+folder+"/"+summary) as file:
            rerun = True
            for line in file:
                if "FAILURES:30" in line:
                    rerun = False
            if rerun:
                print("results/max/"+folder+"/"+summary)