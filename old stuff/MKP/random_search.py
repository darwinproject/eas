# Michael's messing about copy of knapsack.py
import random
import or_reader
import numpy as np

from statsmodels import robust

from deap import base
from deap import creator
from deap import tools

creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)

max_evals = 10000


def valid(individual):
    constraints = getConstraints(individual)
    return all([i < 0 for i in(constraints - CAPACITIES)])


def generateIndividual(container, size):
    ind = [0] * size
    while True:
        index = random.randint(0, size-1)
        ind[index] = 1
        if not valid(ind):
            ind[index] = 0
            return container(ind)


def evalKnapsack(individual):
    if valid(individual):
        return np.dot(individual, values),
    return 0,


def getConstraints(individual):
    return np.dot(coefficients, individual)


def eaOne(population, toolbox, max_evals, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(eval_count < max_evals):
        gen += 1

        offspring = toolbox.population(n=1)

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, 1)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)

    return population, logbook

toolbox.register("evaluate", evalKnapsack)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.2)
toolbox.register("select", tools.selBest)


def main():
    pop = toolbox.population(n=1)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = eaOne(pop, toolbox, max_evals, stats, halloffame=hof)
    return pop, log, hof

if __name__ == "__main__":

    for f in range(1, 10):
        print("mknapcb", f)
        global problems
        problems = or_reader.read('test_data/mknapcb'+str(f)+'.txt',
                                  'test_data/mkcbres.txt', f)
        evals = []
        failures = 0
        random.seed(10000)
        summary = ''

        for i in range(len(problems)):
#        for i in range(1):
            global N
            global M
            global O
            global valies
            global coefficients
            global CAPACITIES
            N, M, O, values, coefficients, CAPACITIES = problems[i]
            toolbox.register("individual", generateIndividual, creator.Individual, N)
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
            toolbox.register("mutate", tools.mutFlipBit, indpb=1/N)#approx one bit flipping per mutation
            pop, log, hof = main()
            evals.append(sum(log.select("nevals")))
            # Set the output file here
            results_file = 'results/Random/'
            with open(results_file+'mknapcb'+str(f)+'-'+str(i)+'.txt', 'w') as l:
                    print(log, file=l)

            if (hof[0].fitness.values[0] < int(O)):
                failures += 1

            print("Run:", i,
                  "Evals:", evals[-1],
                  "MES:", np.median(evals),
                  "MAD:", robust.mad(evals),
                  "FAILURES:", failures,
                  "Best:", hof[0].fitness.values[0],
                  "Solution:", O)

            summary += ("Run:" + str(i) +
                        " Evals:" + str(evals[-1]) +
                        " MES:" + str(np.median(evals)) +
                        " MAD:" + str(robust.mad(evals)) +
                        " FAILURES:" + str(failures) +
                        " Best:" + str(hof[0].fitness.values[0]) +
                        " Solution:" + str(O) + "\n")

        with open(results_file+'mknapcb'+str(f)+'-summary.txt', 'w') as s:
                print(summary, file=s)
