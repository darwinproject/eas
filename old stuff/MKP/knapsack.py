import random

import numpy
import matplotlib.pyplot as plt

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

CAPACITY = 100
NUM_ITEMS = 10


# dictionary of tuples key = item tuple = (weight, value)
# items = {'a':(1, 1), 'b':(1, 1), 'c':(1, 1), 'd':(1, 1), 'e':(1, 1), 'f':(1, 1), 'g':(1, 1), 'h':(1, 1), 'i':(1, 1), 'j':(1, 1)}

# better as a list of tuples (weight, value)
# items = [(10, 10), (20, 25), (15, 10), (5, 15), (50, 40), (30, 35), (15, 20), (25, 30), (40, 20), (10, 5)]

items = [(10, 10), (20, 25), (15, 10), (5, 15), (50, 40), (30, 40), (15, 20), (25, 30), (40, 40), (10, 5)]

# creator.create("Fitness", base.Fitness, weights=(-1.0, 1.0)) #  minimize the weight while maxamizing the value
creator.create("FitnessMax", base.Fitness, weights=(1.0, ))  # minimize the weight while maxamizing the value
creator.create("Individual", list, fitness=creator.FitnessMax)


toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 0, 1)

toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, NUM_ITEMS)

#  Structure initializers

toolbox.register("population", tools.initRepeat, list, toolbox.individual)


def evalKnapsack(individual):
    weight = 0
    value = 0

    for i in range(NUM_ITEMS):
        if individual[i] == 1:
            weight += items[i][0]
            value += items[i][1]

    if weight > CAPACITY:
        # return 10000, 0 # terrible fitness
        return 0, 

    # return weight, value
    return value, 


def eaOne(population, toolbox, ngen, MU, LAMBDA, crossover, stats=None, 
          halloffame=None, verbose=__debug__):

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(1, ngen+1):
        #  Select the next generation individuals
        # offspring = toolbox.select(population, len(population))

        #  Vary the pool of individuals
        # print("pop1" , population)

        # offspring = [toolbox.clone(ind) for ind in population]
        # offspring= toolbox.select(offspring, LAMBDA)
        # if len(offspring)<LAMBDA:
        # for 1+lambda comment out otherwise

        offspring = [toolbox.clone(ind) for ind in population] #* LAMBDA

        # print (offspring)
        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for i in range(len(offspring)):
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values
        # referencing error that needs fixing

        # print(offspring)

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        #  Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # print("pop2", population)
        # print ("off", offspring)
        # print (population+offspring)
        # print (toolbox.select(population+offspring, 1))
        population = toolbox.select(population+offspring, MU)

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

        if gen == ngen:
            print(logbook.select("max"))
            plt.figure(4)
            plt.title("min, avg, max")
            # plt.plot(range(ngen+1), logbook.select("min"), label="min")
            # plt.plot(range(ngen+1),logbook.select("avg"),label="avg")
            plt.plot(range(ngen+1), logbook.select("max"), label="max")
            plt.legend(loc=8)
            plt.axis([0, ngen, 0, 150])
            plt.show()
            maxi = open("max.txt", "a")
            maxi.write(str(logbook.select("max")))
            maxi.write("# ")
            maxi.close()

    return population, logbook

toolbox.register("evaluate", evalKnapsack)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selBest)


def main():
    NGEN = 24
    MU = 20
    LAMBDA = 20
    crossover = True
    # CXPB = 0
    # MUTPB = 1

    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    # stats.register("avg", numpy.mean, axis=0)
    # stats.register("std", numpy.std, axis=0)
    # stats.register("min", numpy.min, axis=0)
    stats.register("max", numpy.max)

    # algorithms.eaMuPlusLambda(pop, toolbox, MU, LAMBDA, CXPB, MUTPB, NGEN, stats, halloffame=hof)

    pop, log = eaOne(pop, toolbox, NGEN, MU, LAMBDA, crossover, stats, halloffame=hof)

    # print (pop)
    # print(hof)
    return pop, stats, hof

if __name__ == "__main__":
    random.seed(1)
    for i in range(30):
        pop, log, hof = main()
        print("Best individual is: %s\nwith fitness: %s" % (hof[0], hof[0].fitness))
        # print (pop)
    # main()
