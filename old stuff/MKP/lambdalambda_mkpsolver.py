#mkpsolver but with James messing about with it to implement 1+lambda,lambda
import random
import or_reader
import numpy as np
import scipy.stats as ss
from statsmodels import robust
import copy
from deap import base
from deap import creator
from deap import tools
import math
creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)
#In all experiments the mutation probability is k/n and the crossover probability 1/k. Except for Fig. 3 we have k = λ

MU = 1
LAMBDA = 8
LAMBDA_CONST=LAMBDA
crossover = True
max_evals = 10000
k=LAMBDA
c=(1/k)
F=1.5
adjust=False

#1+(LAMBDA,LAMBDA)
#Produces lambda offspring by mutation from mu (single parent)
#Selects best offspring, to mate with the parent (gross)

def binomialMutate(p):
    l=np.random.binomial(N,p)
    #print(l)
    return l
def   llOffSpring(population,toolbox,LAMBDA):
        numbers=list(range(0, 99))
        offspring = []#Copy old generators
        l=binomialMutate(LAMBDA/N)
        for i in range (round(LAMBDA)):
            offspring.append(toolbox.clone(population[0]))
            toolbox.mutate(offspring[i],indpb=l/N)
#            list100=np.random.choice(numbers,l)
#            for x in range(len(list100)):
#                index=(list100[x])
#                offspring[i][index]= not offspring[i][index]
#            del offspring[i].fitness.values  
        fitnesses = toolbox.map(toolbox.evaluate, offspring)#
        for ind, fit in zip(offspring, fitnesses):
            ind.fitness.values = fit
            
        bestOne = toolbox.select(offspring,1)[0]
        #print("Bestone:",bestOne, "\n",population[0])
        for j in range(round(LAMBDA)):
            bestOneClone = toolbox.clone(bestOne)
            parentClone = toolbox.clone(population[0])
            toolbox.mateBest(parentClone,bestOneClone)
            offspring[j]=parentClone
            del offspring[j].fitness.values    
        return offspring
# From cb_mkpsolver.py

    
def valid(individual):
    constraints = getConstraints(individual)
    return all([i < 0 for i in(constraints - CAPACITIES)])


def generateIndividual(container, size):
    ind = [0] * size
    while True:
        index = random.randint(0, size-1)
        ind[index] = 1
        if not valid(ind):
            ind[index] = 0
            return container(ind)


# select parents uniformly at random with replacement
def selParents(individuals, k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))

    return parents


def evalKnapsack(individual):
    if valid(individual):
        return np.dot(individual, values),
    return 0,
    
def evalOneMax(individual):
	return sum(individual),

def getConstraints(individual):
    return np.dot(coefficients, individual)



def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit
   
    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    #LAMBDA = int( math.sqrt(math.log(N)))
    #print(N,LAMBDA)
    #print(LAMBDA)
    while(eval_count < max_evals):
       
#        if(gen %20 == 0):
#                    print("evals:",eval_count)
    
        gen += 1
        parent=toolbox.clone(population[0])
        #print(k,p,c)
        offspring = llOffSpring(population,toolbox,LAMBDA)
        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)*2
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

            # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind)*2, **record)

        if halloffame[0].fitness.values[0] >= int(O):
            break
        
        else:
            if(adjust):
                if(population[0].fitness.values<=parent.fitness.values):
                     LAMBDA = min(N,(LAMBDA*(math.pow(F,1/4))))
                     #print("lambda", LAMBDA)
                else:
                    LAMBDA= max(1,(( LAMBDA/F)))
                #if(gen %20 == 0):
#                    print("lambda", LAMBDA)
               #
                k=LAMBDA
                c=(1/k)
                p=k/N
                toolbox.register("mateBest", tools.cxUniform, indpb=c)
                toolbox.register("mutate", tools.mutFlipBit, indpb=p)
#        d=population[0].fitness.values[0]-int(O)
#        d=math.sqrt(math.pow(d,2))
#        LAMBDA = max(1,math.sqrt(int(O)/d))
        #print(N,d,int(O))
        #print(LAMBDA)
    return population, logbook

toolbox.register("evaluate", evalKnapsack)
#toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mateBest", tools.cxUniform, indpb=c)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", selParents)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats,
                     halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":

    for f in range(1, 10):
        print("mknapcb", f)
        global problems
        problems = or_reader.read('test_data/mknapcb'+str(f)+'.txt',
                                  'test_data/mkcbres.txt', f)
        evals = []
        failures = 0
        random.seed(10000)
        summary = ''
      
        for i in range(len(problems)):
            #LAMBDA=LAMBDA_CONST
            global N
            global M
            global O
            global values
            global coefficients
            global CAPACITIES
            N, M, O, values, coefficients, CAPACITIES = problems[i]
            toolbox.register("individual", generateIndividual, creator.Individual, N)
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
            toolbox.register("mutate", tools.mutFlipBit, indpb=LAMBDA/N)#approx one bit flipping per mutation
            pop, log, hof = main()
        
            evals.append(sum(log.select("nevals")))
            # Set the output file here
            if adjust:
                results_file = 'results/'+str(MU)+'+'+"lambda"+","+"lambda"+('GA' if crossover else 'EA')+'/'
            else:
                 results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+","+str(LAMBDA)+('GA' if crossover else 'EA')+'/'
            with open(results_file+'mknapcb'+str(f)+'-'+str(i)+'.txt', 'w') as l:
                    print(log, file=l)

            if (hof[0].fitness.values[0] < int(O)):
                failures += 1

            print("Run:", i,
                  "Evals:", evals[-1],
                  "MES:", np.median(evals),
                  "MAD:", robust.mad(evals),
                  "FAILURES:", failures,
                  "Best:", hof[0].fitness.values[0],
                  "Solution:", O)

            summary += ("Run:" + str(i) +
                        " Evals:" + str(evals[-1]) +
                        " MES:" + str(np.median(evals)) +
                        " MAD:" + str(robust.mad(evals)) +
                        " FAILURES:" + str(failures) +
                        " Best:" + str(hof[0].fitness.values[0]) +
                        " Solution:" + str(O) + "\n")

        with open(results_file+'mknapcb'+str(f)+'-summary.txt', 'w') as s:
                print(summary, file=s)
