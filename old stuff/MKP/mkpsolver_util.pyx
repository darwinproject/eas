# Michael's messing about copy of knapsack.py
import random
import numpy as np
cimport numpy as np
DTYPE = np.int
ctypedef np.int_t DTYPE_t

from deap import tools
from numpy.random import choice
import math

def powerDistribution(int n, float BETA):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob
    
def fmut(int N, float BETA):
    CB=powerDistribution(N, BETA)
    alphas = list(range(1, int(N/2)))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
    draw=choice(alphas,1,p=probs)
    return draw

def valid(np.ndarray individual, np.ndarray capacities, np.ndarray coefficients):
    constraints = np.dot(coefficients, individual)
    return all([i < 0 for i in(constraints - capacities)])

def generateIndividual(container, int size, np.ndarray capacities, np.ndarray coefficients):
    # Start with an empty bag
    ind = np.zeros(size)
    # Add items to the bag until it's not valid
    while valid(ind, capacities, coefficients):
        index = random.randint(0, size-1)
        ind[index] = 1
    # Remove the last item so we've got a valid bag again
    ind[index] = 0
    return container(ind)

# select parents uniformly at random with replacement
def selParents(individuals, int k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))
    return parents

def evalKnapsack(int solution, np.ndarray values, np.ndarray capacities, np.ndarray coefficients, np.ndarray individual):
    if valid(individual, capacities, coefficients):
        return float(np.dot(individual, values)/solution),
    return 0,

def eaOne(population, toolbox, int MU, int LAMBDA, int N, int max_evals=10000,
          crossover=False, stats=None, halloffame=None, verbose=__debug__,
          fast=False, float BETA=1.5):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    while(eval_count < max_evals):
        gen += 1
        if(fast):
               alpha = fmut(BETA)
               toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
        #  Generate offspring
        offspring = [toolbox.clone(ind) for ind in population]
        offspring = toolbox.selectParents(offspring, LAMBDA)

        # if crossover is being used it is done before mutation
        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for i in range(len(offspring)):
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values
            

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)

    return population, logbook