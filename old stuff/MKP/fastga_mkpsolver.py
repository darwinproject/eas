# Michael's messing about copy of knapsack.py
import random
import or_reader
import numpy as np

from statsmodels import robust

from deap import base
from deap import creator
from deap import tools
import math
from numpy.random import choice
creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)

MU = 100
LAMBDA = 1
crossover = False
max_evals = 10000
BETA = 1.5

def powerDistribution(n):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob
    
def fmut():
    CB=powerDistribution(N)
    alphas = list(range(1, int(N/2)))
    #print(N,len(alphas))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
   
    draw=choice(alphas,1,p=probs)
   
    return draw
    
def fmut_test(ind):
    CB=powerDistribution(N)
    print(CB)
    alphas = list(range(1, int(N/2)))
   
    #print(N,len(alphas))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
       # print(alphas[i],[probs[i]])
    draw=choice(alphas,1,p=probs)
    #print(probs[draw-1])
  
    #print("\n", len(probs),len(alphas))
    return draw
# From cb_mkpsolver.py
def valid(individual):
    constraints = getConstraints(individual)
    return all([i < 0 for i in(constraints - CAPACITIES)])


def generateIndividual(container, size):
    ind = [0] * size
    while True:
        index = random.randint(0, size-1)
        ind[index] = 1
        if not valid(ind):
            ind[index] = 0
            return container(ind)


# select parents uniformly at random with replacement
def selParents(individuals, k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))

    return parents


def evalKnapsack(individual):
    if valid(individual):
        return np.dot(individual, values),
    return 0,
def evalOneMax(individual):
	return sum(individual),

def getConstraints(individual):
    return np.dot(coefficients, individual)


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])
    #print(fmut(population[0]))
    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
 
    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(eval_count < max_evals):
        alpha = fmut()
        toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
  
        gen += 1
        #  Generate offspring
        
        offspring = toolbox.selectParents(population, 2)
        offspring = [toolbox.clone(ind) for ind in population]
        offspring = toolbox.mate(offspring[0], offspring[1])
        off, = toolbox.mutate(offspring[0])
        offspring=[]    
        offspring.append(off)
        del offspring[0].fitness.values   
        # if crossover is being used it is done before mutation
        if(eval_count % 1000 ==0):
            print(eval_count)
            

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        
        if halloffame[0].fitness.values[0] >= int(O):
            break

    return population, logbook

toolbox.register("evaluate", evalKnapsack)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", selParents)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats,
                     halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":

    for f in range(1, 10):
        print("mknapcb", f)
        global problems
        problems = or_reader.read('test_data/mknapcb'+str(f)+'.txt',
                                  'test_data/mkcbres.txt', f)
        evals = []
        failures = 0
        random.seed(10000)
        summary = ''

        for i in range(len(problems)):
            global N
            global M
            global O
            global valies
            global coefficients
            global CAPACITIES
            N, M, O, values, coefficients, CAPACITIES = problems[i]
            toolbox.register("individual", generateIndividual, creator.Individual, N)
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
          
            pop, log, hof = main()
            evals.append(sum(log.select("nevals")))
            # Set the output file here
            results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+"Fast"+('GA' if crossover else 'EA')+'/'
            with open(results_file+'mknapcb'+str(f)+'-'+str(i)+'.txt', 'w') as l:
                    print(log, file=l)

            if (hof[0].fitness.values[0] < int(O)):
                failures += 1

            print("Run:", i,
                  "Evals:", evals[-1],
                  "MES:", np.median(evals),
                  "MAD:", robust.mad(evals),
                  "FAILURES:", failures,
                  "Best:", hof[0].fitness.values[0],
                  "Solution:", O)

            summary += ("Run:" + str(i) +
                        " Evals:" + str(evals[-1]) +
                        " MES:" + str(np.median(evals)) +
                        " MAD:" + str(robust.mad(evals)) +
                        " FAILURES:" + str(failures) +
                        " Best:" + str(hof[0].fitness.values[0]) +
                        " Solution:" + str(O) + "\n")

        with open(results_file+'mknapcb'+str(f)+'-summary.txt', 'w') as s:
                print(summary, file=s)
