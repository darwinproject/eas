#cython: boundscheck=False
#cython: wraparound=False
import random
import or_reader
import numpy as np

from statsmodels import robust

from deap import base
from deap import creator
from deap import tools
from numpy.random import choice
import math
import os
import time

t0 = time.time()
creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)

MU = 20
LAMBDA = 20
crossover = False
max_evals = 10000
BETA = 1.5
fast=False

def powerDistribution(n):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob
    
def fmut():
    CB=powerDistribution(N)
    alphas = list(range(1, int(N/2)))
    #print(N,len(alphas))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
   
    draw=choice(alphas,1,p=probs)
   
    return draw
# From cb_mkpsolver.py
def valid(individual):
    constraints = getConstraints(individual)
    return all([i < 0 for i in(constraints - capacities)])

def generateIndividual(container, size):
    ind = [0] * size
    while True:
        index = random.randint(0, size-1)
        ind[index] = 1
        if not valid(ind):
            ind[index] = 0
            return container(ind)


# select parents uniformly at random with replacement
def selParents(individuals, k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))

    return parents


def evalKnapsack(individual):
    if valid(individual):
        return np.dot(individual, values),
    return 0,
def evalOneMax(individual):
	return sum(individual),

def getConstraints(individual):
    return np.dot(coefficients, individual)


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(eval_count < max_evals):
        gen += 1

        if(fast):
               alpha = fmut()
               toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
        #  Generate offspring
        offspring = [toolbox.clone(ind) for ind in population]
        offspring = toolbox.selectParents(offspring, LAMBDA)

        # if crossover is being used it is done before mutation
        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for i in range(len(offspring)):
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values
            

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)

        if halloffame[0].fitness.values[0] >= int(O):
            break

    return population, logbook

toolbox.register("evaluate", evalKnapsack)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.2)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", selParents)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats,
                     halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":

    # for f in range(1, 10):
    for f in range(1, 10):
        global problems
        problems = or_reader.read('test_data/mknapcb'+str(f)+'.txt',
                                  'test_data/mkcbres.txt', f)
        evals = []
        failures = 0
        random.seed(10000)
        summary = ''

        # for i in range(len(problems)):
        for i in range(1):
            global N
            global M
            global O
            global valies
            global coefficients
            global capacities
            N, M, O, values, coefficients, capacities = problems[i]
            toolbox.register("individual", generateIndividual, creator.Individual, N)
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
            toolbox.register("mutate", tools.mutFlipBit, indpb=1/N)#approx one bit flipping per mutation
            pop, log, hof = main()
            evals.append(sum(log.select("nevals")))
            # Set the output file here
            results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+('Fast' if fast else '')+('GA' if crossover else 'EA')+'/'
            if not os.path.exists(results_file):
                os.makedirs(results_file)
            with open(results_file+'mknapcb'+str(f)+'-'+str(i)+'.txt', 'w') as l:
                    print(log, file=l)

            if (hof[0].fitness.values[0] < int(O)):
                failures += 1

            print("Run:", i,
                  "Evals:", evals[-1],
                  "MES:", np.median(evals),
                  "MAD:", robust.mad(evals),
                  "FAILURES:", failures,
                  "Best:", hof[0].fitness.values[0],
                  "Solution:", O)

            summary += ("Run:" + str(i) +
                        " Evals:" + str(evals[-1]) +
                        " MES:" + str(np.median(evals)) +
                        " MAD:" + str(robust.mad(evals)) +
                        " FAILURES:" + str(failures) +
                        " Best:" + str(hof[0].fitness.values[0]) +
                        " Solution:" + str(O) + "\n")

        with open(results_file+'mknapcb'+str(f)+'-summary.txt', 'w') as s:
                print(summary, file=s)
    t1 = time.time()
    print(t1-t0)