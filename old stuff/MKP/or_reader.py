# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 11:39:31 2017

@author: Michael, James (JP helped me too)
"""

import numpy as np


def read(file, soln_file, problemNum):
    scores = read_scores(soln_file)
    problem = scores[problemNum-1]

    probs = []
    with open(file, 'r') as f:
        num_probs = int(f.readline().strip())
        nump = 0
        while(nump < num_probs):  # While there are problems to be read

            while True:
                result = (f.readline().strip().split())
                if (len(result) > 0):
                    break
            n, m, o = list(map(float, result))
            n = int(n)
            m = int(m)
            o = int(o)
            if(o == 0):
                o = int(problem[nump])

            eq = (n + (n * m) + m)
            data = []
            nump += 1

            for line in f:
                line = line.strip()
                data += [float(x) for x in line.split()]
                if(len(data) >= eq):
                    break

            data = data[0:]
            p = np.array(data[:n])  # Object values (works)
            data = data[n:]
            b = np.array(data[int(m)*-1:])  # Knapsack capacities
            data = data[:int(m)*-1]
            r = np.array([data[i:i+n] for i in range(0, len(data), n)])
            tup = (n, m, o, p, r, b)
            probs.append(tup)
        return probs


def read_scores(file):
    scores = []
    problems = []
    old_problem = ""
    with open(file, 'r') as f:
        count = 0
        for line in f:
                line = line.strip().split()
                if(line):
                    problem = line[0].split("-")[0]

                    if(problem != old_problem):
                        old_problem = problem
                        if(count != 0):
                            problems.append(scores)
                        count += 1
                        scores = []

                    scores.append(line[1])
    return problems


def printinfo(n, m, o, p, r, b):
        print("n = ", n, "m = ", m, "o = ", o)
        print("p (values)", p)
        print("r (weights)", r)
        print("b (capacities)", b)
        print("=================================================")


def convertToBetter():
    solutions = {}
    with open('test_data/mkcbres.txt') as s:
        for line in s:
            k, v = line.split()
            solutions[k] = float(v)
    for i in range(1, 10):
        probs = read('test_data/mknapcb'+str(i)+'.txt', 'test_data/mkcbres.txt', 1)
        counter = 0
        for i, prob in enumerate(probs):
            N, M, O, values, coefficients, capacities = prob
            soln = str(M)+'.'+str(N)+'-'+str(i).zfill(2)
            print(soln)
            O = solutions[soln]
            with open('test_data2/MKP_'+str(N)+'_'+str(M)+'_'+str(counter)+'.txt', 'w') as f:
                f.write(str(N)+' '+str(M)+' '+str(O))
                f.write('\n')
                f.write(str(values.tolist()).replace('[', '').replace(']', '').replace(',', ''))
                f.write('\n')
                for lst in coefficients.tolist():
                    f.write(str(lst).replace('[', '').replace(']', '').replace(',', ''))
                    f.write('\n')
                f.write('\n')
                f.write(str(list(capacities)).replace('[', '').replace(']', '').replace(',', ''))
                
                counter += 1

#convertToBetter()
