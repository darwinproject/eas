# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 16:41:45 2017

@author: Matthew
"""
import random
import or_reader
import numpy as np
import matplotlib.pyplot as plt

from statsmodels import robust

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from numpy.random import choice
from operator import sub
from operator import add

import math

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)
BETA=1.5
toolbox = base.Toolbox()

# toolbox.register("attr_bool", random.randint, 0, 1)

# randomly add items to knapsack until item doesnt fit then go back to last
# valid knapsack and return it
def powerDistribution(n):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob
    
def fmut():
    CB=powerDistribution(N)
    alphas = list(range(1, int(N/2)))
    #print(N,len(alphas))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))
   
    draw=choice(alphas,1,p=probs)
   
    return draw
    
def generateIndividual(container, size):
    ind = [0] * size
    while True:
        index = random.randint(0, size-1)
        ind[index] = 1
        if not valid(ind):
            ind[index] = 0
            return container(ind)


def valid(individual):
    constraints = getConstraints(individual)
    return all([i < 0 for i in(constraints - CAPACITIES)])


def getConstraints(individual):
    return np.dot(coefficients, individual)


# needs work, would be more efficient if weight is calculated and updated through
# method instead of calling valid
def repair(ind):
    constraints = getConstraints(ind)
    # drop phase
    while any([constraints[i] > CAPACITIES[i] for i in range(M)]):
        inx = next(i for i, v in enumerate(ind) if v == 1)
        ind[inx] = 0
        constraints = list(map(sub, constraints, [i[inx] for i in coefficients]))

    # add phase
    for i in range(N):
        if ind[i] == 0:
            constraints_temp = list(map(add, constraints, [c[i] for c in coefficients]))
            if not any([constraints_temp[i] > CAPACITIES[i] for i in range(M)]):
                ind[i] = 1
                constraints = constraints_temp
    return ind


def fitnessFunction(individual):
    if valid(individual):
        return np.dot(individual, values),
    return 0,


# returns one offspring as algorithm is designed to only create one new
# individual per generation
def crossoverAndMutation(population, toolbox):
    offspring = [toolbox.clone(ind) for ind in population]
    offspring = toolbox.mate(offspring[0], offspring[1])
    offspringM = toolbox.mutate(offspring[0])
    return offspringM[0]


def eaOne(population, toolbox, max_evals, MU, stats=None, halloffame=None):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # get initial fitnesses
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(fitness_count < max_evals):
        # select 2 parents using binary tournament selection
#        alpha = fmut()
#        toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
        offspring = toolbox.selectParents(population, 2)

        # apply crossover and mutation to get one offspring
        offspring = crossoverAndMutation(offspring, toolbox)
        # use repair operator on offspring
        offspring = repair(offspring)

        # if offspring is duplicate discard and go to next generation
        if offspring in population:
            continue

        del offspring.fitness.values
        # evaluate fitness of offspring (can use much less code as only one
        # individual)

        fitness = toolbox.evaluate(offspring)
        offspring.fitness.values = fitness
        fitness_count += 1
        gen += 1

        # the child thing is necessary as deap is expecting a list of lists
        # (i.e. a pop) not just an individual
        child = []
        child.append(offspring)

        if halloffame is not None:
            halloffame.update(child)

        population = toolbox.select(population+child, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=1, **record)

    return population, logbook

toolbox.register("evaluate", fitnessFunction)
toolbox.register("mate", tools.cxUniform, indpb=0.5)

toolbox.register("selectParents", tools.selTournament, tournsize=2)
toolbox.register("select", tools.selBest)


def main():
    max_evals = 10000
    MU = 100
    # Lambda is always 1

    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, stats, halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":
    for f in range(1, 10):
        print("mknapcb", f)
        global problems
        problems = or_reader.read('test_data/mknapcb'+str(f)+'.txt',
                                  'test_data/mkcbres.txt', f)
        evals = []
        failures = 0
        random.seed(10000)
        summary = ''

        for i in range(len(problems)):
            global N
            global M
            global O
            global valies
            global coefficients
            global CAPACITIES
            N, M, O, values, coefficients, CAPACITIES = problems[i]
            toolbox.register("individual", generateIndividual, creator.Individual, N)
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
            toolbox.register("mutate", tools.mutFlipBit, indpb=1/N)#approx one bit flipping per mutation
            pop, log, hof = main()
            evals.append(sum(log.select("nevals")))
            # Set the output file here
            results_file = 'results/CB100FastRepair/'
            with open(results_file+'mknapcb'+str(f)+'-'+str(i)+'.txt', 'w') as l:
                    print(log, file=l)

            if (hof[0].fitness.values[0] < int(O)):
                failures += 1

            print("Run:", i,
                  "Evals:", evals[-1],
                  "MES:", np.median(evals),
                  "MAD:", robust.mad(evals),
                  "FAILURES:", failures,
                  "Best:", hof[0].fitness.values[0],
                  "Solution:", O)

            summary += ("Run:" + str(i) +
                        " Evals:" + str(evals[-1]) +
                        " MES:" + str(np.median(evals)) +
                        " MAD:" + str(robust.mad(evals)) +
                        " FAILURES:" + str(failures) +
                        " Best:" + str(hof[0].fitness.values[0]) +
                        " Solution:" + str(O) + "\n")

        with open(results_file+'mknapcb'+str(f)+'-summary.txt', 'w') as s:
                print(summary, file=s)
