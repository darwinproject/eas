# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 10:22:02 2017

@author: Michael
"""

import os
import pandas as pd
import numpy as np
from statsmodels import robust

folder = "Random"
files = sorted([f for f in os.listdir('results/'+folder) if "summary" not in f])

for size in range(0, len(files), 10):
    evals = []
    summary = {}
    for file in files[size:size+10]:
        print(file)
        with open('results/'+folder+'/'+file) as f:
            headings = [s.strip() for s in f.readline().split()]
            rows = []
            for r in f:
                row = [s.strip() for s in r.split()]
                if len(row) == len(headings):
                    rows.append([float(r) for r in row])

            table = pd.DataFrame(rows, columns=headings)
            fails = table[table['max'] < 1]['gen'].count()
            nevals = int(sum(table.head(fails+1)['nevals'].values))
            evals.append(nevals)
            run = int(int(file.split("_")[3].split(".")[0]) / 20)
            summary[run] = ("Run:" + str(run) +
                            " Evals:" + str(nevals) +
                            " MES:" + str(np.median(evals)) +
                            " MAD:" + str(robust.mad(evals)) +
                            " Failure:" + str(int(max(table['max'].values) < 1)) +
                            " Best:" + str(max(table['max'].values)) +
                            " Solution:" + str(1))
    s = str(file.split("_")[2].split(".")[0])
    with open('results/'+folder+'/'+'IsingSpinGlass_'+s+'_summary.txt', 'w') as f:
        for x in range(10):
            print(summary[x], file=f)
    try:
        os.remove('results/'+folder+'/'+'IsingSpinGlass-summary-'+s+'.txt')
    except:
        print('FAILED TO DELETE IsingSpinGlass-summary-'+s+'.txt')

    print()