#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 17:50:46 2017

@author: michael
"""

import numpy as np

def read(file):
    with open(file, 'r') as f:
        min_energy, solution = f.readline().split(' ')
        min_energy = int(min_energy)
        solution = [int(x) for x in solution.strip()]
        number_of_spins = int(f.readline())
        spins = []
        for line in f:
            spins.append([int(x) for x in line.split(' ')])
        spins = np.array(spins)
        solution = np.array(solution)
        return min_energy, solution, number_of_spins, spins