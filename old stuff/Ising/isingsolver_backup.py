# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np
from isingreader import read

from statsmodels import robust

from os import listdir
from numpy.random import choice
from deap import base
from deap import creator
from deap import tools
import math
import time
t0 = time.time()

bit_to_sign = [-1, 1]
fast=True
MU =1
LAMBDA = 1
crossover = False
max_evals = 10000
precision = 65536
BETA = 1.5

creator.create("FitnessMin", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMin)


toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 0, 1)

def selParents(individuals, k):
    parents = []
    for i in range(k):
        parents.append(random.choice(individuals))
    return parents


def powerDistribution(n):
    prob = 0;
    for i in range(1,round(n/2)):
        prob+=(math.pow(i,-BETA))
    return prob

def fmut(BETA):
    CB=powerDistribution(N)
    alphas = list(range(1, int(N/2)))
    #print(N,len(alphas))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB,-1)*math.pow(alphas[i],-BETA))

    draw=choice(alphas,1,p=probs)

    return draw

def float_round(value, precision):
    return round(value * precision) / precision


def evalIsing(individual):
    energy = 0
    for spin in spins:
        energy -= (bit_to_sign[individual[spin[0]]] * spin[2] *
                   bit_to_sign[individual[spin[1]]])
    return float_round(1 - (energy - min_energy) / span, precision),


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    eval_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(eval_count < max_evals):
        gen += 1
        if(fast):
               alpha = fmut(BETA)
               toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
        #  Generate offspring
        offspring = [toolbox.clone(ind) for ind in population]
        offspring = toolbox.selectParents(offspring, LAMBDA)

        # if crossover is being used it is done before mutation
        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for i in range(len(offspring)):
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values
            

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        eval_count += len(invalid_ind)
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)

    return population, logbook

    return population, logbook

toolbox.register("evaluate", evalIsing)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", selParents)

def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats, halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    for i, f in enumerate(sorted(listdir('test_data'))[:10]):
        global min_energy
        global solution
        global number_of_spins
        global spins
        global span
        global N

        min_energy, solution, number_of_spins,  spins = read('test_data/'+f)
        span = number_of_spins - min_energy

        toolbox.register("evaluate", evalIsing)


        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attr_bool, len(solution))
        N= len(solution)
        #  Structure initializers
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        pop, log, hof = main()
        # Set the output file here

        results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+('Fast' if fast else '')+('GA' if crossover else 'EA')+'/'

        with open(results_file+f, 'w') as l:
                print(log, file=l)
        evals.append(sum(log.select("nevals")))
#        print (log.select("nevals"))
#        print (evals)
        if (evalIsing(hof[0]) != 1.0):
            failures += 1

        print("Run:", i,
              "Evals:", evals[-1],
              "MES:", np.median(evals),
              "MAD:", robust.mad(evals),
              "FAILURES:", failures,
              "Best:", hof[0].fitness.values[0],
              "Solution:", 1.0)
        summary += ("Run:" + str(i) +
                " Evals:" + str(evals[-1]) +
                " MES:" + str(np.median(evals)) +
                " MAD:" + str(robust.mad(evals)) +
                " FAILURES:" + str(failures) +
                " Best:" + str(hof[0].fitness.values[0]) +
                " Solution:1.0\n")
        if counter % 10 == 9:
                problem_size = len(solution)
            # Set the output file here as well
                with open(results_file+'IsingSpinGlass-summary-'+str(problem_size)+'.txt', 'w') as s:
                    print(summary, file=s)
                summary = ''

        counter += 1

    t1 = time.time()
    print(t1-t0)

