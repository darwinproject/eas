# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np
from isingreader import read

from statsmodels import robust

import os
from deap import base
from deap import creator
from deap import tools

import ising_util as util

import time
t0 = time.time()

fast = False
MU = 1
LAMBDA = 1
crossover = False
max_evals = 100
BETA = 1.5

creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", np.ndarray, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", util.selParents)

if __name__ == "__main__":
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    for i, f in enumerate(sorted(os.listdir('test_data'))[:1]):
        f = 'IsingSpinGlass_pm_784_0.txt'
        length = int(f.split('_')[2])

        min_energy, solution, number_of_spins,  spins = read('test_data/'+f)
        span = number_of_spins - min_energy

        toolbox.register("evaluate", util.evalIsing, spins, min_energy, span)
        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attr_bool, len(solution))

        #  Structure initializers
        toolbox.register("population", tools.initRepeat, list,
                         toolbox.individual)
        pop, log, hof = util.main(length, MU, LAMBDA, toolbox,
                                  crossover=crossover, fast=fast)

        # Set the output folder here
        results_folder = ('results/' + str(MU) + '+' + str(LAMBDA) +
                          ('Fast' if fast else '') +
                          ('GA' if crossover else 'EA') + '/')
        if not os.path.exists(results_folder):
            os.makedirs(results_folder)

        with open(results_folder+f, 'w') as l:
                print(log, file=l)
        evals.append(sum(log.select("nevals")))

        if (hof[0].fitness.values[0] < 1):
            failures += 1

        # Print result
        result = ("Run:" + str(i) +
                  " Evals:" + str(evals[-1]) +
                  " MES:" + str(np.median(evals)) +
                  " MAD:" + str(robust.mad(evals)) +
                  " FAILURES:" + str(failures) +
                  " Best:" + str(hof[0].fitness.values[0]) +
                  " Solution:1.0")
        print(result)

        # Save result to file
        summary += (result + '\n')

        if counter % 10 == 9:
            # Set the output file
                with open(results_folder + 'IsingSpinGlass-summary-' +
                          str(length) + '.txt', 'w') as s:
                    print(summary, file=s)
                summary = ''

        counter += 1

    t1 = time.time()
    print(t1-t0)
