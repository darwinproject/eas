#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 16:58:55 2017

@author: michael
"""
# cython: profile=True
import random

import numpy as np
cimport numpy as np
DTYPE = np.int
ctypedef np.int_t DTYPE_t

from numpy.random import choice
from deap import base
from deap import tools
import math
from functools import reduce

cdef int precision = 65536

def powerDistribution(int n, float BETA):
    return reduce((lambda x, y: x + (math.pow(y,-BETA))), range(1,round(n/2)))

def fmut(int N, float BETA):
    CB=powerDistribution(N, BETA)
    alphas = list(range(1, int(N/2)))
    probs = [(math.pow(CB,-1)*math.pow(alphas[i],-BETA)) for i in range(len(alphas))]
    draw=choice(alphas,1,p=probs)
    return draw

def selParents(individuals, int k, toolbox):
    parents =  [random.choice(individuals) for i in range(k)]
    return [toolbox.clone(ind) for ind in parents]

def float_round(float value, int precision):
    return round(value * precision) / precision


def evalIsing(np.ndarray[DTYPE_t, ndim=2] spins, int min_energy, int span,np.ndarray[DTYPE_t, ndim=1] individual):
    cdef np.ndarray[DTYPE_t, ndim=1] bit_to_sign = np.array([-1, 1])
    energy = - sum([(bit_to_sign[individual[spin[0]]] *
               spin[2] *
               bit_to_sign[individual[spin[1]]]) for spin in spins])
    return float_round(1.0 - (energy - min_energy) / span, precision),

def main(N, MU, LAMBDA, toolbox, crossover=False, fast=False):
    population = toolbox.population(n=MU)
    hof = tools.HallOfFame(1, similar=np.array_equal)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(population, toolbox, MU, LAMBDA, N, stats=stats,
                          halloffame=hof, crossover=crossover, fast=fast)

    return pop, log, hof

def eaOne(population, toolbox, int MU, int LAMBDA, int N, crossover=False,
          stats=None, halloffame=None, verbose=__debug__, fast=False,
          int max_evals=10000, float BETA=1.5):
    
    gen = 0
    
    # initialise logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # initialise individuals fitness
    eval_count = len(population)
    fitnesses = toolbox.map(toolbox.evaluate, population)
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit

    # initialise hall of fame
    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(population), **record)

    # Begin the generational process
    while(eval_count < max_evals):
        gen += 1
        if(fast):
               alpha = fmut(N, BETA)
               toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/N)
        
        #  Generate offspring
        offspring = toolbox.selectParents(population, LAMBDA, toolbox)

        # if crossover is being used it is done before mutation
        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for off in offspring:
            off, = toolbox.mutate(off)
            del off.fitness.values
            

        #  Evaluate the individuals with an invalid fitness
        eval_count += len(offspring)
        fitnesses = toolbox.map(toolbox.evaluate, offspring)
        for ind, fit in zip(offspring, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(offspring), **record)

    return population, logbook