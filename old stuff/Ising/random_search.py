# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np
from isingreader import read

from statsmodels import robust

from os import listdir

from deap import base
from deap import creator
from deap import tools

bit_to_sign = [-1, 1]

max_evals = 10000
precision = 65536

creator.create("FitnessMin", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMin)
toolbox = base.Toolbox()


def generateIndividual(container, size):
    ind = [random.randint(0, 1) for x in range(size+1)]
    return container(ind)


def float_round(value, precision):
    return round(value * precision) / precision


def evalIsing(individual):
    energy = 0
    for spin in spins:
        energy -= (bit_to_sign[individual[spin[0]]] * spin[2] *
                   bit_to_sign[individual[spin[1]]])

    return float_round(1 - (energy - min_energy) / span, precision),


def eaOne(population, toolbox, max_evals, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    while(fitness_count < max_evals):
        gen += 1

        offspring = toolbox.population(n=1)

        #  Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fitness_count += len(invalid_ind)

        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        #  Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, 1)

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
    return population, logbook

toolbox.register("evaluate", evalIsing)
toolbox.register("select", tools.selBest)


def main():
    pop = toolbox.population(n=1)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = eaOne(pop, toolbox, max_evals, stats, halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    for i, f in enumerate(sorted(listdir('test_data'))):
        global min_energy
        global solution
        global number_of_spins
        global spins
        global span
        min_energy, solution, number_of_spins,  spins = read('test_data/'+f)
        toolbox.register("individual", generateIndividual, creator.Individual, len(solution))

        #  Structure initializers
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        span = number_of_spins - min_energy
        pop, log, hof = main()
        # Set the output file here
        results_file = 'results/Random/'
        with open(results_file+f, 'w') as l:
                print(log, file=l)
        evals.append(sum(log.select("nevals")))
        if (evalIsing(hof[0]) != evalIsing(solution)):
            failures += 1

        print("Run:", i,
              "Evals:", evals[-1],
              "MES:", np.median(evals),
              "MAD:", robust.mad(evals),
              "FAILURES:", failures,
              "Best:", hof[0].fitness.values[0],
              "Solution:", evalIsing(solution)[0])

        if counter % 10 == 9:
            problem_size = len(solution)
            # Set the output file here as well
            with open(results_file+'IsingSpinGlass_'+str(problem_size)+'_summary.txt', 'w') as s:
                print(summary, file=s)
            summary = ''
        summary += ("Run:" + str(i) +
                    " Evals:" + str(evals[-1]) +
                    " MES:" + str(np.median(evals)) +
                    " MAD:" + str(robust.mad(evals)) +
                    " FAILURES:" + str(failures) +
                    " Best:" + str(hof[0].fitness.values[0]) +
                    " Solution:" + str(evalIsing(solution)[0]) + "\n")
        counter += 1
    with open(results_file+'IsingSpinGlass_'+str(problem_size)+'_summary.txt', 'w') as s:
        print(summary, file=s)
