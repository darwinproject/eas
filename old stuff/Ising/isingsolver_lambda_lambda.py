# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np
from isingreader import read

from statsmodels import robust

from os import listdir

from deap import base
from deap import creator
from deap import tools
import math
bit_to_sign = [-1, 1]
precision = 2

MU = 1
LAMBDA = 1
crossover = True
adjusting = True
max_evals = 10000
precision = 65536
k = LAMBDA
c = 1/k
F = 2.5
creator.create("FitnessMin", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMin)


toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 0, 1)


def binomialMutate(p):
    return np.random.binomial(len(solution), p)


def llOffSpring(population, toolbox, LAMBDA):
        offspring = []  # Copy old generators
        binom = binomialMutate(LAMBDA/len(solution))
        numbers = list(range(0, len(solution)-1))
        for i in range(round(LAMBDA)):
            offspring.append(toolbox.clone(population[0]))
            list100 = np.random.choice(numbers, binom)
            for x in range(len(list100)):
                index = list100[x]
                offspring[i][index] = not offspring[i][index]
            del offspring[i].fitness.values
        fitnesses = toolbox.map(toolbox.evaluate, offspring)
        for ind, fit in zip(offspring, fitnesses):
            ind.fitness.values = fit
        bestOnes = toolbox.select(offspring, 1)
        bestOne = bestOnes[0]
        for j in range(round(LAMBDA)):
            bestOneClone = toolbox.clone(bestOne)
            parentClone = toolbox.clone(population[0])
            toolbox.mateBest(parentClone, bestOneClone)
            offspring[j] = parentClone
            del offspring[j].fitness.values
        return offspring


def float_round(value, precision):
    return round(value * precision) / precision


def evalIsing(individual):
    energy = 0
    for spin in spins:
        energy -= (bit_to_sign[individual[spin[0]]] * spin[2] *
                   bit_to_sign[individual[spin[1]]])

    return float_round(1 - (energy - min_energy) / span, precision),


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    N = (len(solution))
    # Begin the generational process
    while(fitness_count < max_evals):
        parent = toolbox.clone(population[0])
        offspring = llOffSpring(population, toolbox, LAMBDA)
        gen += 1

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fitness_count += len(invalid_ind)*2

        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        #  Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)
        population = toolbox.select(population+offspring, MU)
        if adjusting:
            if(parent.fitness.values[0] >= population[0].fitness.values[0]):
                LAMBDA = min(N, (LAMBDA * (math.pow(F, 1/4))))
            else:
                LAMBDA = max(1, (LAMBDA/F))
            k = LAMBDA

            c = (1 / k)

            toolbox.register("mateBest", tools.cxUniform, indpb=c)
            toolbox.register("mutate", tools.mutFlipBit, indpb=(k/N))

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind)*2, **record)

    return population, logbook


toolbox.register("evaluate", evalIsing)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=k/1)
toolbox.register("select", tools.selBest)
toolbox.register("mateBest", tools.cxUniform, indpb=c)
toolbox.register("rando", tools.selRandom)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats,
                     halloffame=hof)

    return pop, log, hof


if __name__ == "__main__":
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    for i, f in enumerate(sorted(listdir('test_data'))):
        global min_energy
        global solution
        global number_of_spins
        global spins
        global span
        min_energy, solution, number_of_spins,  spins = read('test_data/'+f)
        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attr_bool, len(solution))

        #  Structure initializers
        toolbox.register("population", tools.initRepeat, list,
                         toolbox.individual)

        span = number_of_spins - min_energy
        pop, log, hof = main()
        # Set the output file here
        results_file = ('results/' + str(MU) + '+' + "lambda,lambda" +
                        ('GA' if crossover else 'EA')+'/')
        with open(results_file+f, 'w') as logfile:
                print(log, file=logfile)
        evals.append(sum(log.select("nevals")))
        if (evalIsing(hof[0]) != evalIsing(solution)):
            failures += 1

        print("Run:", i,
              "Evals:", evals[-1],
              "MES:", np.median(evals),
              "MAD:", robust.mad(evals),
              "FAILURES:", failures,
              "Best:", hof[0].fitness.values[0],
              "Solution:", evalIsing(solution)[0])
        summary += ("Run:" + str(i) +
                    " Evals:" + str(evals[-1]) +
                    " MES:" + str(np.median(evals)) +
                    " MAD:" + str(robust.mad(evals)) +
                    " FAILURES:" + str(failures) +
                    " Best:" + str(hof[0].fitness.values[0]) +
                    " Solution:" + str(evalIsing(solution)[0]) + "\n")
        if counter % 10 == 9:
                problem_size = len(solution)
                print(counter, problem_size)
            # Set the output file here as well
                with open((results_file + 'IsingSpinGlass-summary-' +
                           str(problem_size)+'.txt', 'w')) as s:
                    print(summary, file=s)
                summary = ''
        counter += 1
