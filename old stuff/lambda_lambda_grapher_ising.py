# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 14:03:53 2017

@author: Michael
"""

import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

def  interpolate(folder,problem_size):
    folder = "ising/results/"+folder+"/"
    tables = []
    columns = []
    solutions = []
    solution =1
#    with open("mkp/test_data/mkcbres.txt") as mkcbres:
#        solns = []
#        for line in mkcbres:
#            if line.strip() == "":
#                continue
#            l = line.split()
#            problem = int(l[0].split("-")[1])
#            soln = float(l[1])
#            solns.append(soln)
#            if problem == 29:
#                solutions.append(solns)
#                solns = []
    cols = ['gen', 'nevals', 'avg', 'std', 'min', 'max']
    #problems = [f for f in os.listdir(results_folder) if "summary" not in f][29*(pf-1):29*pf]
    averages = pd.DataFrame(columns=cols)
    count=0
    for filename in [f for f in os.listdir(folder) if "summary" not in f]:
        if "_"+str(problem_size)+"_" not in filename:
                continue
        if("summary" not in filename):
            print(filename)
            with open(folder+filename) as f:
                headings = [s.strip() for s in f.readline().split()]
                print(headings)
                rows = []
                for r in f:

                    row = [s.strip() for s in r.split()]
                    if len(row) == len(headings):
                       # print("yo")
                        rows.append([float(r) for r in row])

                table = pd.DataFrame(rows, columns=headings)
                table['nevals'] = table.nevals.cumsum()
                table['avg'] = table['avg']/solution
                table['min'] = table['min']/solution
                table['max'] = table['max']/solution
                tables.append(table)
                count+=1
                #print(count)

                #plt.plot(table['nevals'], table['max'], '-', label="Average")

    print(count)
    val_set = set()
    for t in tables:
        val_set.update(t.nevals.values)
    print("Interpolating")
    t = 0
    while t < len(tables):
        for v in val_set:
            if v not in tables[t].nevals.values:
                tables[t] = pd.concat([tables[t],pd.DataFrame(data=[[np.NAN, v, np.NAN, np.NAN, np.NAN, np.NAN]], columns=headings)])
                tables[t].sort_values(by=['nevals'], inplace=True)
                tables[t] = tables[t].interpolate()
        tables[t] = pd.DataFrame(columns=headings, data=tables[t].values)
        t += 1
        print("Interpolated: ", t, "of: ",len(tables))
    averages = pd.DataFrame(columns=['fitness', 'nevals'])
    for t in tables:
        averages = pd.concat([averages, t])

    table = averages.groupby(level=0).mean()
    return table
#print(table.tail())
#table.sort_values(by=['avg'], inplace=True)
#plt.rcParams["font.family"] = "CMU Serif"
#plt.title("Title")
#plt.xlabel("Fitness evaluations")
#plt.ylabel("Fitness")
#
#plt.plot(table['nevals'], table['max'], '-', color='#089FFF', label="Average")
#plt.fill_between(table['nevals'], table['min'], table['max'],
#                 alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
#                 label="Min max")
#plt.fill_between(table['nevals'], table['avg']-table['std'],
#                 table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
#                 antialiased=True, linewidth=0, label="Standard deviation")
#plt.legend(loc=0)
#table= interpolate("1+lambda,lambdaGA",1)
#plt.plot(table['nevals'], table['avg'], '-',  linewidth=2, alpha=0.7, label="Average: "+"lambda")
###
###    plt.fill_between(table['nevals'], table['min'], table['max'],
###                     alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
###                     label="Min max")
###    plt.fill_between(table ['nevals'], table['avg']-table['std'],
###                     table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
###                     antialiased=True, linewidth=0, label="Standard deviation")
##plt.legend(loc=0)
##
#plt.show()

