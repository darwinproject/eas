
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 14:01:39 2017

@author: Michael
#Summed up nevals so we could use them for the graph instead of gens - James
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import lambda_lambda_grapher as llg
import seaborn as sns
sns.set_context(rc={"lines.linewidth": 1.2})
sns.set_style("whitegrid")
test1="1+8,8GA"
test2="1+10EA"
test3="100+1EA"
test4="20+20GA"
test5="1+1EA"
test6="CB100Repair"
test7="CB100NoRepair"
test8="10+1EA"
test9="1+1FASTEA"
test10="1+lambda,lambdaGA"
test11="20+20FastGA"
test0="Random"
tests=[test0,test5,test9,test4,test11]
tests=[test10]
pf =2
#colors=["#089FFF","#FF000F","#00FF22","#124452"]

color=iter(cm.rainbow(np.linspace(0,1,len(tests))))
solutions = []
with open("mkp/test_data/mkcbres.txt") as mkcbres:
    solns = []
    for line in mkcbres:
        if line.strip() == "":
            continue
        l = line.split()
        problem = int(l[0].split("-")[1])
        soln = float(l[1])
        solns.append(soln)
        if problem == 29:
            solutions.append(solns)
            solns = []



for i in range(len(tests)):
    c=next(color)
    results_folder = "mkp/results/"+tests[i]+"/"
    averages = pd.DataFrame(columns=['gen', 'nevals', 'avg', 'std', 'min', 'max'])
    n=3
    for filename in [f for f in os.listdir(results_folder) if "summary" not in f][29*(pf-1):29*pf]:
        file = int(filename[7])-1
        problem = int(filename.split("-")[1].split(".")[0])
        solution = solutions[file][problem]
        print(filename, solution)
        with open(results_folder+filename) as f:
            headings = [s.strip() for s in f.readline().split("\t")]
            rows = []
            for r in f:
                row = [s.strip() for s in r.split("\t")]
                if len(row) == len(headings):
                    rows.append([float(r) for r in row])

            table = pd.DataFrame(rows, columns=headings)
            table['nevals'] = table.nevals.cumsum()
            table['avg'] = table['avg']/solution
            table['min'] = table['min']/solution
            table['max'] = table['max']/solution
            #print(table)
            averages = pd.concat([averages, table])

    #print(averages)

    table = averages.groupby(level=0).mean()
    #print(table)
    nevals = table['nevals'].values
    s_nevals = sorted(nevals)
    eq = [nevals[i] == s_nevals[i] for i in range(len(nevals))]

#    first_row = pd.DataFrame(columns=["gen", "nevals", "avg", "std", "min", "max"])
#    first_row.loc[0] = [0, 0, 0, 0, 0, 0]
#    table = pd.concat([first_row, table])

    plt.rcParams["font.family"] = "CMU Serif"
    plt.title("Comparison of Different Algorithms on MKP")
    plt.xlabel("Fitness Function evaluations")
    plt.ylabel("Fitness")
    n=1
    if("lambda,lambda" in tests[i]):
        table=llg.interpolate(tests[i],pf)
    plt.plot(table['nevals'], table['avg'], '-', c=c,  linewidth=2, alpha=0.5, label="Average: "+tests[i])
#
#    plt.fill_between(table['nevals'], table['min'], table['max'],
#                     alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
#                     label="Min max")
#    plt.fill_between(table ['nevals'], table['avg']-table['std'],
#                     table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
#                     antialiased=True, linewidth=0, label="Standard deviation")
    plt.legend(loc=0,frameon=True)

plt.show()

