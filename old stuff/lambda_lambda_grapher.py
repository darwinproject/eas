# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 14:03:53 2017

@author: Michael
"""

import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

def  interpolate(folder,pf):
    folder = "MKP/results/"+folder
    tables = []
    columns = []
    
    solutions = []
    with open("mkp/test_data/mkcbres.txt") as mkcbres:
        solns = []
        for line in mkcbres:
            if line.strip() == "":
                continue
            l = line.split()
            problem = int(l[0].split("-")[1])
            soln = float(l[1])
            solns.append(soln)
            if problem == 29:
                solutions.append(solns)
                solns = []
    
    for filename in os.listdir(folder) [29*(pf-1):29*pf]:
        if("summary" not in filename):
            file = int(filename[7])-1
            problem = int(filename.split("-")[1].split(".")[0])
            solution = solutions[file][problem]
            print(filename, solution)
            if "summary" in filename:
                continue
            with open(folder+"/"+filename) as f:
                columns = f.readline().split()
                rows = []
                for line in f:
                    row = [float(x) for x in line.split()]
                    rows.append(row)
                table = pd.DataFrame(data=rows, columns=columns)
                table['nevals'] = table.nevals.cumsum()
                table['avg'] = table['avg']/solution
                table['min'] = table['min']/solution
                table['max'] = table['max']/solution
                tables.append(table)
                #plt.plot(table['nevals'], table['max'], '-', label="Average")
    
    
    val_set = set()
    for t in tables:
        val_set.update(t.nevals.values)
    print("Interpolating")
    t = 0
    while t < len(tables):
        for v in val_set:
            if v not in tables[t].nevals.values:
                tables[t] = pd.concat([tables[t],pd.DataFrame(data=[[np.NAN, v, np.NAN, np.NAN, np.NAN, np.NAN]], columns=columns)])
                tables[t].sort_values(by=['nevals'], inplace=True)
                tables[t] = tables[t].interpolate()
        tables[t] = pd.DataFrame(columns=columns, data=tables[t].values)
        t += 1
        print("Interpolated: ", t, "of: ",len(tables))
    averages = pd.DataFrame(columns=['fitness', 'nevals'])
    for t in tables:
        averages = pd.concat([averages, t])
    
    table = averages.groupby(level=0).mean()
    return table
#print(table.tail())
#table.sort_values(by=['avg'], inplace=True)
#plt.rcParams["font.family"] = "CMU Serif"
#plt.title("Title")
#plt.xlabel("Fitness evaluations")
#plt.ylabel("Fitness")
#
#plt.plot(table['nevals'], table['max'], '-', color='#089FFF', label="Average")
#plt.fill_between(table['nevals'], table['min'], table['max'],
#                 alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
#                 label="Min max")
#plt.fill_between(table['nevals'], table['avg']-table['std'],
#                 table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
#                 antialiased=True, linewidth=0, label="Standard deviation")
#plt.legend(loc=0)
plt.show()
