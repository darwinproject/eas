# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 14:01:39 2017

@author: Michael
#Summed up nevals so we could use them for the graph instead of gens - James
"""

import os
import pandas as pd
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
import numpy as np
import lambda_lambda_grapher_ising as llg
import p3_grapher as p3
import seaborn as sns
sns.set_context(rc={"lines.linewidth": 1.2})
sns.set_style("whitegrid")
test0="Random"
test1="1+1FastEA"
test2="1+1EA"
test3="20+5EA"
test4="20+20GA"
test5="100+1EA"
test6="1+lambda,lambdaGA"
test7="P3"
test8="Random"
test11="20+20FastGA"
tests=[test7,test11,test4,test1,test6,test2,test5,test0]
#tests=[test0]
#tests=[test6]
#results_folder = "ising/results/1+lambda,lambdaGA"
problems=[16,64,784]
problem_size = 784
for p in problems:
    problem_size=p
    color=iter(cm.rainbow(np.linspace(0,1,len(tests))))
    cols = ['gen', 'nevals', 'avg', 'std', 'min', 'max']
    plt.figure()
    for i in range(len(tests)):
        c=next(color)
        results_folder = "ising/results/"+tests[i]+"/"
        #problems = [f for f in sorted(os.listdir(results_folder)) if "summary" not in f and "_"+str(problem_size)+"_" in f and f!=None]
        averages = pd.DataFrame(columns=cols)
        if("P3" in tests[i]):
            table=p3.interpolate(tests[i],problem_size)
        elif("lambda,lambda" in tests[i]):
            table=llg.interpolate(tests[i],problem_size)
        else:
       
            for filename in [f for f in os.listdir(results_folder) if "summary" not in f]:
                if "_"+str(problem_size)+"_" not in filename:
                    continue
                print(filename)
                with open(results_folder+filename) as f:
                    headings = [s.strip() for s in f.readline().split()]
                    #print(headings)
                    rows = []
                    for r in f:
                        row = [s.strip() for s in r.split()]
                        if len(row) == len(headings):
                            rows.append([float(r) for r in row])
            
                    table = pd.DataFrame(rows, columns=headings)
                    averages = pd.concat([averages, table])
            
            table = averages.groupby(level=0).mean()
            table['nevals'] = table.nevals.cumsum()
            first_row = pd.DataFrame(columns=["gen", "nevals", "avg", "std", "min", "max"])
            first_row.loc[0] = [0, 0, 0, 0, 0, 0]
            table = pd.concat([first_row, table])
            table = table[table['max'] < 1]
        
        plt.rcParams["font.family"] = "CMU Serif"
        title="Comparison of different algorithms on the ising problem \n Problem Size: "+str(problem_size)
        plt.title(title)
        plt.xlabel("Fitness evaluations")
        plt.ylabel("Fitness")
        if("P3" in tests[i]):
            plt.plot(table['nevals'], table['fitness'], '-',  linewidth=2, alpha=0.5, color=c, label="Average "+tests[i])
        else:
            plt.plot(table['nevals'], table['avg'], '-',  linewidth=2, alpha=0.5, color=c, label="Average "+tests[i])
    #    plt.fill_between(table['nevals'], table['min'], table['max'],
    #                     alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
    #                     label="Min max")
    #    plt.fill_between(table['nevals'], table['avg']-table['std'],
    #                     table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
    #                     antialiased=True, linewidth=0, label="Standard deviation")
        plt.legend(loc=0,frameon=True)
plt.show()


