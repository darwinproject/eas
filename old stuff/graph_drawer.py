# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 16:42:29 2017

@author: Michael
"""
import pandas as pd
import matplotlib.pyplot as plt
from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfile

master = Tk()

Label(master, text="Graph title").pack()
title = Entry(master)
title.pack()

Label(master, text="x label").pack()
x_v = StringVar()
x_v.set("Generation number")
x = Entry(master, textvariable=x_v)
x.pack()

Label(master, text="y label").pack()
y_v = StringVar()
y_v.set("Fitness value")
y = Entry(master, textvariable=y_v)
y.pack()

Label(master, text="Source").pack()
source = StringVar()
s = Entry(master, textvariable=source)
s.pack()

def open_file():
    filename = askopenfilename(filetypes=[("Text files", "*.txt")])
    source.set(filename)

Button(master, text="Open", width=10, command=open_file).pack()


def callback():
    with open(source.get()) as f:
        headings = [s.strip() for s in f.readline().split("\t")]
        rows = []
        for r in f:
            row = [s.strip() for s in r.split("\t")]
            if len(row) == len(headings):
                rows.append([float(r) for r in row])

        table = pd.DataFrame(rows, columns=headings)
        print(table)

        plt.rcParams["font.family"] = "CMU Serif"
        plt.title(title.get())
        plt.xlabel(x.get())
        plt.ylabel(y.get())
        plt.plot(table['gen'], table['avg'], '-', color='#089FFF', label="Average")
        plt.fill_between(table['gen'], table['min'], table['max'],
                         alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
                         label="Min max")
        plt.fill_between(table['gen'], table['avg']-table['std'],
                         table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
                         antialiased=True, linewidth=0, label="Standard deviation")
        plt.legend(loc=0)
        plt.show()


Button(master, text="Show", width=10, command=callback).pack()

mainloop()
