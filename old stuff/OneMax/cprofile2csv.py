#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 15:26:25 2017

@author: michael
"""

with open('profile.txt', 'r') as f:
    with open('profile.csv', 'w') as o:
        for line in f:
            line = line.strip()
            columns = line.split()
            for i in range(5):
                print(columns[i]+",", file=o)

