# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np

from statsmodels import robust

import os
from numpy.random import choice
from deap import base
from deap import creator
from deap import tools
import math

MU = 2
LAMBDA = 1
crossover = False
max_evals = 100
BETA = 1.5


creator.create("FitnessMin", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 0, 1)


def evalOneMax(individual):
    return float(sum(individual))/len(individual),


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    while(fitness_count < max_evals):
        if (population[0].fitness.values[0] == population[1].fitness.values[0]):
            offspring = toolbox.clone(population[random.randint(0, 1)])
            offspring2 = toolbox.clone(population[random.randint(0, 1)])
            offspring, offspring2 = toolbox.mate(offspring, offspring2)
            offspring, = toolbox.mutate(offspring)
            del offspring.fitness.values
            offspring.fitness.values = toolbox.evaluate(offspring)
        elif (population[0].fitness.values[0] > population[1].fitness.values[0]):
            offspring = toolbox.clone(population[0])
            offspring, = toolbox.mutate(offspring)
            del offspring.fitness.values
            offspring.fitness.values = toolbox.evaluate(offspring)
        else:
            offspring = toolbox.clone(population[1])
            offspring, = toolbox.mutate(offspring)
            del offspring.fitness.values
            offspring.fitness.values = toolbox.evaluate(offspring)

        offList = [offspring]
        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offList)
            
        # same fitness so tie break
        if (population[0].fitness.values[0] == population[1].fitness.values[0] and population[0].fitness.values[0] == offList[0].fitness.values[0]):
            # all identical so it doesnt matter which 2
            if (population[0] == population[1] and population[0] == offList[0]):
                # population doesn't change
                break
            # offspring is unique
            elif (population[0] == population[1]):
                population[1] = offList[0]
        # different fitness so just select best
        else:
            population = toolbox.select(population+offList, MU)

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        print(logbook.stream)
        if max(logbook.select("max")) == 1.0:
            break

        fitness_count += 1

    return population, logbook

toolbox.register("evaluate", evalOneMax)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selBest)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats, halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":
    random.seed()
    evals = []
    failures = 0
    summary = ''
    counter = 0
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, 100)
      #  Structure initializers
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    pop, log, hof = main()

#==============================================================================
#     results_file = 'results/'+str(MU)+'+'+str(LAMBDA)+('Fast' if fastGA else '')+('GA' if crossover else 'EA')+'/'
#     if lambdalambda:
#         results_file = 'results/'+str(MU)+'+'+"lambda"+","+"lambda"+('GA' if crossover else 'EA')+'/'
# 
#     if not os.path.exists(results_file):
#         os.makedirs(results_file)
# 
#     for i, f in enumerate(sorted(os.listdir('../Ising/test_data'))):
#         global length
#         length = int(f.split('_')[2])
#         toolbox.register("individual", tools.initRepeat, creator.Individual,
#                          toolbox.attr_bool, length)
#         #  Structure initializers
#         toolbox.register("population", tools.initRepeat, list, toolbox.individual)
# 
#         pop, log, hof = main()
#         # Set the output file here
# 
#         with open(results_file+"OneMax_"+str(length) + "_" + str(i%20), 'w') as l:
#                 print(log, file=l)
#         evals.append(sum(log.select("nevals")))
#         if (evalOneMax(hof[0])[0] != 1.0):
#             failures += 1
# 
#         print("Run:", i,
#               "Evals:", evals[-1],
#               "MES:", np.median(evals),
#               "MAD:", robust.mad(evals),
#               "FAILURES:", failures,
#               "Best:", hof[0].fitness.values[0],
#               "Solution:", 1)
# 
#         if counter % 10 == 9:
#             problem_size = length
#             # Set the output file here as well
#             with open(results_file+'OneMax-summary-'+str(problem_size)+'.txt', 'w') as s:
#                 print(summary, file=s)
#             summary = ''
#         summary += ("Run:" + str(i) +
#                     " Evals:" + str(evals[-1]) +
#                     " MES:" + str(np.median(evals)) +
#                     " MAD:" + str(robust.mad(evals)) +
#                     " FAILURES:" + str(failures) +
#                     " Best:" + str(hof[0].fitness.values[0]) +
#                     " Solution:1\n")
#         counter += 1
#     with open(results_file+'OneMax-summary-'+str(problem_size)+'.txt', 'w') as s:
#         print(summary, file=s)
#==============================================================================
