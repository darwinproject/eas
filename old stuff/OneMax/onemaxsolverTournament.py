# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random

import numpy as np

from statsmodels import robust

from os import listdir
from numpy.random import choice
from deap import base
from deap import creator
from deap import tools
import math

fastGA = True
MU = 1
LAMBDA = 1
crossover = False
max_evals = 10000
BETA = 1.5

creator.create("FitnessMin", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 0, 1)


def powerDistribution(n):
    prob = 0
    for i in range(1, round(n/2)):
        prob += (math.pow(i, -BETA))
    return prob


def fmut():
    CB = powerDistribution(length)
    alphas = list(range(1, int(length/2)))
    probs = []
    for i in range(len(alphas)):
        probs.append(math.pow(CB, -1)*math.pow(alphas[i], -BETA))
    draw = choice(alphas, 1, p=probs)
    return draw


def evalOneMax(individual):
    return float(sum(individual))/len(individual),


def eaOne(population, toolbox, max_evals, MU, LAMBDA, crossover, stats=None,
          halloffame=None, verbose=__debug__):
    gen = 0
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitness_count = len(invalid_ind)
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)

    # Begin the generational process
    # for gen in range(1, ngen+1):
    while(fitness_count < max_evals):
        if(fastGA):
            alpha = fmut()
            toolbox.register("mutate", tools.mutFlipBit, indpb=alpha/length)
        gen += 1
        offspring = [toolbox.clone(ind) for ind in population]

        if crossover:
            for i in range(1, len(offspring), 2):
                offspring[i-1], offspring[i] = toolbox.mate(offspring[i-1], offspring[i])
                del offspring[i - 1].fitness.values, offspring[i].fitness.values

        for i in range(len(offspring)):
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values

        #  Evaluate the individuals with an invalid fitness
        offspring = toolbox.select(offspring, LAMBDA)
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fitness_count += len(invalid_ind)

        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        #  Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        population = toolbox.select(population+offspring, MU)

        #  Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
#        print(logbook.stream)
#        if max(logbook.select("max")) == 1.0:
#            break

    return population, logbook

toolbox.register("evaluate", evalOneMax)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selTournament, tournsize=3)


def main():
    pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    pop, log = eaOne(pop, toolbox, max_evals, MU, LAMBDA, crossover, stats, halloffame=hof)

    return pop, log, hof

if __name__ == "__main__":
    random.seed(1000)
    evals = []
    failures = 0
    summary = ''
    counter = 0

    for i, f in enumerate(sorted(listdir('../Ising/test_data'))):
        global length
        length = int(f.split('_')[2])
        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attr_bool, length)
        #  Structure initializers
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        pop, log, hof = main()
        # Set the output file here
        results_file = 'results/'+str(MU)+'+'+str(LAMBDA) + ('Fast' if fastGA else '') + ('GA' if crossover else 'EA')+'/'
        with open(results_file+"OneMax_"+str(length) + "_" + str(i%20), 'w') as l:
                print(log, file=l)
        evals.append(sum(log.select("nevals")))
        if (evalOneMax(hof[0])[0] != 1.0):
            failures += 1

        print("Run:", i,
              "Evals:", evals[-1],
              "MES:", np.median(evals),
              "MAD:", robust.mad(evals),
              "FAILURES:", failures,
              "Best:", hof[0].fitness.values[0],
              "Solution:", 1)

        if counter % 10 == 9:
            problem_size = length
            # Set the output file here as well
            with open(results_file+'OneMax-summary-'+str(problem_size)+'.txt', 'w') as s:
                print(summary, file=s)
            summary = ''
        summary += ("Run:" + str(i) +
                    " Evals:" + str(evals[-1]) +
                    " MES:" + str(np.median(evals)) +
                    " MAD:" + str(robust.mad(evals)) +
                    " FAILURES:" + str(failures) +
                    " Best:" + str(hof[0].fitness.values[0]) +
                    " Solution:1\n")
        counter += 1
    with open(results_file+'OneMax-summary-'+str(problem_size)+'.txt', 'w') as s:
        print(summary, file=s)
