# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 14:01:39 2017

@author: Michael
#Summed up nevals so we could use them for the graph instead of gens - James
"""

import os
import pandas as pd
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
import numpy as np
import lambda_lambda_grapher_ising as llg
import p3_grapher as p3
import seaborn as sns
import re
sns.set_context(rc={"lines.linewidth": 1.2})
sns.set_style("whitegrid")
test0="Random"
test1="1+1FastEA"
test2="1+1EA"
test3="20+5EA"
test4="20+20GA"
test5="100+1EA"
test6="1+lambda,lambdaGA"
test7="P3"
test8="Random"
test11="20+20FastGA"
test12="50+1GA"
test13="10+1GA"
test14="20+5GA"
tests=[test12, test13, test14, test4]
#tests=[test0]
#tests=[test6]
#results_folder = "ising/results/1+lambda,lambdaGA"
problems=[65536,1048576,16777216]
ln = len(problems)
plt.figure(0)
titl="Comparison of algorithm performance over all 3 problem sizes for MGS"
plt.title(titl)
plt.xlabel("Fitness evaluations")
plt.ylabel("Length of Solution Set")
count = 1
col = iter(cm.rainbow(np.linspace(0,1,ln)))
for pl in range(len(tests)):
   plt.figure(pl+1)
   titl="Lengths of optimum solution for different problem sizes and lengths by: "+str(tests[pl])
   plt.title(titl)
   plt.xlabel("Elements in problem set")
   plt.ylabel("Length of Solution Set")
   count+=1
thing = 1
bestFound=[[] for x in range(len(problems))]
cou=0
for p in problems:
    
    bestFound[cou] = [[] for y in range(len(tests))]
    problem_size=p
    color=iter(cm.rainbow(np.linspace(0,1,len(tests))))
    cols = ['gen', 'nevals', 'avg', 'std', 'min', 'max']
    cols1 = ['size', 'min']
    plt.figure(count)
    cl = next(col)
    coun = 0
    for i in range(len(tests)):
        c=next(color)
        results_folder = "MGS/results/"+tests[i]+"/"
        #problems = [f for f in sorted(os.listdir(results_folder)) if "summary" not in f and "_"+str(problem_size)+"_" in f and f!=None]
        averages = pd.DataFrame(columns=cols)
        
        
        for filename in [f for f in os.listdir(results_folder) if "summary" not in f]:
            if str(problem_size) not in filename:
                continue
            print(filename)
            fileTitle = filename.split("_n-")
            de= int(re.findall('\d+', fileTitle[1])[0])
            with open(results_folder+filename) as f:
                headings = [s.strip() for s in f.readline().split()]
                #print(headings)
                rows = []
                for r in f:
                    row = [s.strip() for s in r.split()]
                    if len(row) == len(headings):
                        rows.append([float(r) for r in row])
                bestFound[cou][coun].append((de, rows[-1][4]))
                table = pd.DataFrame(rows, columns=headings)
                averages = pd.concat([averages, table])
                    
                    
            
            table = averages.groupby(level=0).mean()
            table['nevals'] = table.nevals.cumsum()
            first_row = pd.DataFrame(columns=["gen", "nevals", "avg", "std", "min", "max"])
            first_row.loc[0] = [0, 0, 0, 0, 0, 0]
            table = pd.concat([first_row, table])
        
        plt.rcParams["font.family"] = "CMU Serif"
        title="Comparison of different algorithms on the mgs problem \n Problem Size: "+str(problem_size)
        plt.title(title)
        plt.xlabel("Fitness evaluations")
        plt.ylabel("Length of Solution Set")
        plt.plot(table['nevals'], table['avg'], '-',  linewidth=2, alpha=0.5, color=c, label="Average "+tests[i])
        plt.figure(0)      
        if i==0:
            plt.plot(table['nevals'], table['avg'], '-',  linewidth=2, alpha=0.5, color=cl, label="Problem size: "+str(p))
        else:
            plt.plot(table['nevals'], table['avg'], '-',  linewidth=2, alpha=0.5, color=cl, label='_nolegend_')
        plt.figure(count)
        
        coun+=1
            
        
    #    plt.fill_between(table['nevals'], table['min'], table['max'],
    #                     alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
    #                     label="Min max")
    #    plt.fill_between(table['nevals'], table['avg']-table['std'],
    #                     table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
    #                     antialiased=True, linewidth=0, label="Standard deviation")
    cou+=1
    count += 1
    
#print(bestFound)
for b in range(len(bestFound[0])):
    plt.figure(thing)
    color1=iter(cm.rainbow(np.linspace(0,1,len(problems))))
    
    for p in range(len(bestFound)):
        bf = sorted(bestFound[p][b], key=lambda tup:tup[0]) 
        co=next(color1)
        sizes = [x[0] for x in bf]
        mins = [y[1] for y in bf]
        plt.plot(sizes, mins, '-x', linewidth=2, alpha=0.5, color=co, label="Problem size: "+str(problems[p]))
    thing +=1
    
plt.figure(0)
plt.xlim(xmax=10000)
plt.legend(loc=0,frameon=True)
plt.savefig("jptest0.svg")
thin = 0
for j in range(len(tests)+1):
    plt.figure(j)
    plt.ylim(ymin=20)  
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=2,
               ncol=3, mode="expand", borderaxespad=0.)
    plt.savefig("jptest"+str(j)+".svg") 
         
    thin+=1
for i in range(thin,count):
    plt.figure(i)   
    plt.xlim(xmax=10000)
    plt.ylim(ymin=20+(7*(i-thin)))  
    plt.legend(loc=0,frameon=True)  
    plt.savefig("jptest"+str(i)+".svg")
plt.show()


