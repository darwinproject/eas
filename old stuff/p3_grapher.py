# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 14:03:53 2017

@author: Michael
"""

import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

problem_size = 16
folder = "results/1+1EA"
tables = []

def interpolate(folder,pf):
    count=0
    folder = "Ising/results/"+folder +"/"
    for file in os.listdir(folder):
        
        if "_"+str(pf)+"_" not in file:
            continue
        print(file)
        if "summary" in file:
            continue
     
        with open(folder+"/"+file) as f:
            rows = []
            count+=1
            for line in f:
                row = [float(x) for x in line.split()]
                if len(row) == 2:
                    rows.append(row)
                    
    
           
            table = pd.DataFrame(data=rows, columns=['fitness', 'nevals'])
            tables.append(table)

    val_set = set()
    for t in tables:
        val_set.update(t.nevals.values)
    print("Interpolating")
    t = 0
    while t < len(tables):
        for v in val_set:
            if v not in tables[t].nevals.values:
                tables[t] = pd.concat([tables[t],pd.DataFrame(data=[[np.NAN, v]], columns=['fitness', 'nevals'])])
                tables[t].sort_values(by=['nevals'], inplace=True)
                tables[t] = tables[t].interpolate()
        tables[t] = pd.DataFrame(columns=['fitness', 'nevals'], data=tables[t].values)
        t += 1
    
    averages = pd.DataFrame(columns=['fitness', 'nevals'])
    for t in tables:
        averages = pd.concat([averages, t])
    
    table = averages.groupby(level=0).mean()
    print(table.tail())
    table.sort_values(by=['nevals'], inplace=True)
##    plt.rcParams["font.family"] = "CMU Serif"
##    plt.title("Title")
##    plt.xlabel("Fitness evaluations")
##    plt.ylabel("Fitness")
    
#    plt.plot(table['nevals'], table['fitness'], '-', color='#089FFF', label="Average")
    #plt.fill_between(table['nevals'], table['min'], table['max'],
    #                 alpha=0.2, facecolor='#089FFF', antialiased=True, linewidth=0,
    #                 label="Min max")
    #plt.fill_between(table['nevals'], table['avg']-table['std'],
    #                 table['avg']+table['std'], alpha=0.2, facecolor='green',#089FFF',
    #                 antialiased=True, linewidth=0, label="Standard deviation")
    #plt.legend(loc=0)
#    plt.show()
    return table
