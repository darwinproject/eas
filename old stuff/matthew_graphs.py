# -*- coding: utf-8 -*-
"""
Created on Wed Apr  5 15:21:51 2017

@author: Matthew
James: Applied seaborn, changed it so that tests can be applied easily for easier comparison
"""
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
from astropy.table import Table
import seaborn as sns

sns.set_context(rc={"lines.linewidth": 1.2})
sns.set_style("whitegrid")

#sns.set(font_scale=6)wELL
problem="ising"
folder = "results/tables/"+problem
test1="1+(lambda,lambda)GA"
test2="P3"
test8="CB100 (Repair)"
testfcb="CB100Fast (Repair)"
test3="50+1EA"
test4="20+20GA"
test5="1+1FastEA"
test6="20+20FastGA"
test7="1+1EA"
test9="CB100 (No Repair)"
testm="100+1EA"
test11="20+20EA"
testbnb="BnB"
test0="Random"
testfr1="1+1EA (Repair)"
testfr2="1+1FastEA (Repair)"
testfr3="1+(lambda,lambda)GA (Repair)"
testfr4="20+20GA (Repair)"
testfr5="20+20FastRepairGA"
#tests=[test3,test1,test4,test5,test6,test7,test8,test9 ]
simpletests=[test4,test7,testm,test0]
complextests=[test6,test5,test1,test0]
mkp_repair = [test8,testfr4,testbnb,testfr1,testfr2,testfr3,test0]
mkp_norepair=[test9,test4,test5,test7,test1,testm,test0]
ising=[test2,test1,test5,testm,test7,test4,test0]
#tests=[test2,test6,test5,test1]
random_test=[test0]
#tests=[test2,test1,test5,test6]
tests=ising
#tests=ising
def ising_graphtests():
    my_xticks=[]
   
    plt.figure(figsize=(12, 8))
    for i in range(len(tests)):
        tab = Table.read(folder + '/'+tests[i]+ '.tex',ignore_latex_commands=['toprule','midrule','bottomrule'])
        average=[]
        for j in range(len(tab)):
             my_xticks.append(str(int(tab[j][0])))
             try:
                 average.append(tab[j][1])
             except:
                 continue
        plt.rcParams["font.family"] = "CMU Serif"
       
        plt.title("Algorithm performance on the Ising Spin Glass Problem",fontsize=18,y=1.04, color='black')
    
        plt.xlabel("Problem Size",fontsize=16)
        plt.ylabel("Average Fitness",fontsize=16)
        plt.yticks(fontsize=16)
        plt.ylim(0.6,1.0)
        plt.xticks(range(len(my_xticks)),my_xticks, rotation='vertical',fontsize=14)
        plt.plot(average, label="Average "+tests[i])
    plt.tight_layout()

    plt.rcParams["font.family"] = "CMU Serif"
    plt.legend(frameon=True,fontsize=14,loc=3)
    plt.savefig("isingtest.svg")
    plt.show()
    
def mkp_graphtests():
    my_xticks=[]
    plt.figure(figsize=(12, 8))
    for i in range(len(tests)):
        tab = Table.read(folder + '/'+tests[i]+ '.tex',ignore_latex_commands=['toprule','midrule','bottomrule'])
        average=[]
        for j in range(len(tab)):
             my_xticks.append(" " +str(float(tab[j][2]))+", "+str(int(tab[j][0]))+ ", " + str(int(tab[j][1])) )
              
            
             try:
                 average.append(tab[j][3])
             except:
                 continue
        plt.rcParams["font.family"] = "CMU Serif"
        plt.title("Algorithm performance on the Multidimensional Knapsack Problem",fontsize=16,y=1.04)
        plt.xlabel("Problem Size (Tightness Ratio, Constraints, Variables,)",fontsize=16)
        plt.ylabel("Average Fitness",fontsize=16)
        plt.ylim(0.75,1.0)
        plt.yticks(fontsize=16)
        plt.xticks(range(len(my_xticks)),my_xticks, rotation='vertical',fontsize=14)
        plt.plot(average, label="Average "+tests[i],alpha=0.9)
    plt.tight_layout()
    
    plt.rcParams["font.family"] = "CMU Serif"
    plt.legend(loc=0,frameon=True,fontsize=14)
    plt.savefig("mkptest.svg")
    plt.savefig("mkptest.png")
    plt.show()   
    
    
def graph():
    tab = Table.read(folder + '/P3.tex',ignore_latex_commands=['toprule','midrule','bottomrule'])
    tab2 = Table.read(folder + '/50+1EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule'])
    tab3 = Table.read(folder + '/20+20EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule'])
    #size=[]
    average=[]
    average2=[]
    average3=[]
    my_xticks=[]
    
    
    for i in range(len(tab)):
        my_xticks.append(str(int(tab[i][0])))
        #size.append(tab[i][0])
        average.append(tab[i][1])
        average2.append(tab2[i][1])
        average3.append(tab3[i][1])
    #loop jamming for that efficiency
        
    #for i in range(len(size)):
        #my_xticks.append(str(int(size[i])))
        
    plt.rcParams["font.family"] = "CMU Serif"
    plt.title("P3")
    plt.xlabel("Problem Size")
    plt.ylabel("Average Fitness")
    plt.ylim(0.6,1.0)
    plt.xticks(range(25),my_xticks, rotation='vertical')
    plt.plot(average)
    #plt.plot(average2)    
    #plt.plot(average3)        

def averages():
    tab =[]
    tab.append(Table.read(folder + '/1+1EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/10+1EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/10+1GA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/20+5EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/20+5GA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/20+20EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/20+20GA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/50+1EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/100+1EA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/P3.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/Random.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))
    tab.append(Table.read(folder + '/1+lambda,lambdaGA.tex',ignore_latex_commands=['toprule','midrule','bottomrule']))

    for i in range(11):
        print(sum(tab[i]['Average Best'])/25)

if __name__ == "__main__":
    if(problem=="ising"):
        ising_graphtests()
    else:
        mkp_graphtests()
    #averages()