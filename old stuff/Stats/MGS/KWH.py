# -*- coding: utf-8 -*-
"""
Created on Sun Apr 06 03:22:08 2017

@author: George O'Brien
"""
import scipy.stats as sp
import glob
import matplotlib.pyplot as plt
import numpy as np

def mean(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return (sum(results)/len(results))

def dataFromFile(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return results
    
def compareDistribution(file1,file2):
    x=dataFromFile(file1)
    y=dataFromFile(file2)
    if(x!=y):
        return sp.kruskal(x,y,alternative="greater")
    else:
        return ("NA",1)
        
def plotCertainty(results):
    notSignificantPairs=[]
    certainty=np.zeros((len(results)+1,len(results)+1))
    for file1 in range(len(results)):
        for file2 in range(len(results)):
            u,p=compareDistribution(results[file1],results[file2])
            certainty[file1+1][file2+1]=p
            if (p<0.01 and file1!=file2):
                print("Comparing",results[file1]," and",results[file2],"gives a u of",u,"and a p of",p)
                print("Therefore there is a significant difference")
                notSignificantPairs.append([results[file1],results[file2]])
    plt.imshow(certainty, cmap='hot', interpolation='nearest')
    return notSignificantPairs


files=[]
for fileName in glob.glob('*-summary.txt'):
    files+=[fileName]
certainty=np.zeros((len(files)+1,len(files)+1))

EA=[]
for fileName in glob.glob('*EA-summary.txt'):
    EA+=[fileName]

for fileName in glob.glob('*EAMax-summary.txt'):
    EA+=[fileName]

GA=[]
for fileName in glob.glob('*GA-summary.txt'):
    GA+=[fileName]

for fileName in glob.glob('*GAMax-summary.txt'):
    GA+=[fileName]

Repair=[]
for fileName in glob.glob('*Repair-summary.txt'):
    Repair+=[fileName]

Random=[]
for fileName in glob.glob('*Random-summary.txt'):
    Random+=[fileName]

x=1 #print(files)
for file in files:
    file=dataFromFile(file)
 
#plotCertainty(files)
#print(files)    
#plotCertainty(EA)
#print(len(EA))
#u,p=sp.kruskal(dataFromFile(files[0]),dataFromFile(files[1]),dataFromFile(files[2]),dataFromFile(files[3]),dataFromFile(files[4]),dataFromFile(files[5]),dataFromFile(files[6]),dataFromFile(files[7]))
#plotCertainty(GA)
#print(len(GA))
#u,p=sp.kruskal(dataFromFile(files[0]),dataFromFile(files[1]),dataFromFile(files[2]),dataFromFile(files[3]),dataFromFile(files[4]),dataFromFile(files[5]),dataFromFile(files[6]))
u,p=sp.kruskal(dataFromFile(files[0]),dataFromFile(files[1]),dataFromFile(files[2]),alternative="greater")
#print(notSignificantPairs)
#print(len(files))
#u,p=sp.kruskal(dataFromFile(files[0]),dataFromFile(files[1]),dataFromFile(files[2]),dataFromFile(files[3]),dataFromFile(files[4]),dataFromFile(files[5]),dataFromFile(files[6]),dataFromFile(files[7]),dataFromFile(files[8]),dataFromFile(files[9]),dataFromFile(files[10]),dataFromFile(files[11]),dataFromFile(files[12]),dataFromFile(files[13]),dataFromFile(files[14]),dataFromFile(files[15]),dataFromFile(files[16]),dataFromFile(files[17]),dataFromFile(files[18]),dataFromFile(files[19]))
print((1-p)*100)