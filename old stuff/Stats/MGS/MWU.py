# -*- coding: utf-8 -*-
"""
Created on Sun Apr 06 03:22:08 2017

@author: George O'Brien
"""
import scipy.stats as sp
import glob
import matplotlib.pyplot as plt
import numpy as np

def mean(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return (sum(results)/len(results))

def dataFromFile(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return results
    
def compareDistribution(file1,file2):
    x=dataFromFile(file1)
    y=dataFromFile(file2)
    if(x!=y):
        return sp.mannwhitneyu(x,y,alternative="greater")
    else:
        return ("NA",1)
        
def plotCertainty(results):
    notSignificantPairs=[]
    certainty=np.zeros((len(results)+1,len(results)+1))
    for file1 in range(len(results)):
        for file2 in range(len(results)):
            u,p=compareDistribution(results[file1],results[file2])
            certainty[file1+1][file2+1]=p
            if (True):#(p<0.05 and file1!=file2):
                print("Comparing",results[file1]," and",results[file2],"gives a u of",u,"and a p of",p)
                print("Therefore there is a significant difference")
                notSignificantPairs.append([results[file1],results[file2]])
    fig, ax = plt.subplots(1,1)
    ax.imshow(certainty, cmap='hot', interpolation='nearest')
    return notSignificantPairs


files=[]
for fileName in glob.glob('*-summary.txt'):
    files+=[fileName]
certainty=np.zeros((len(files)+1,len(files)+1))


Max=[]
for fileName in glob.glob('*Max-summary.txt'):
    Max+=[fileName]

EA=[]
for fileName in glob.glob('*EA-summary.txt'):
    EA+=[fileName]

for fileName in glob.glob('*EAMax-summary.txt'):
    EA+=[fileName]

GA=[]
for fileName in glob.glob('*GA-summary.txt'):
    GA+=[fileName]

for fileName in glob.glob('*GAMax-summary.txt'):
    GA+=[fileName]

Repair=[]
for fileName in glob.glob('*Repair-summary.txt'):
    Repair+=[fileName]

Random=[]
for fileName in glob.glob('*Random-summary.txt'):
    Random+=[fileName]

x=1 #print(files)
for file in files:
    file=dataFromFile(file)
 
#plotCertainty(files)
#print(files)    
#plotCertainty(Max)
#print(Max)
plotCertainty(EA)
print(EA)
#plotCertainty(GA)
#print(GA)
#plotCertainty(Repair)
#print(Repair)
#print(notSignificantPairs)
