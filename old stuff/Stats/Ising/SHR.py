# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 10:54:26 2017

@author: George O'Brien
"""
import glob
import pandas as pd
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm
from statsmodels.graphics.factorplots import interaction_plot
import matplotlib.pyplot as plt
from scipy import stats
import scipy as sp
import numpy as np

def dataFromFile(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return results
    
def compareDistribution(label1, label2, files,labels):
    ranks=sp.stats.rankdata(files)
    fileRanks=files
    for r in range(len(ranks)):
        fileRanks[(r%(len(fileRanks)))][(r//(len(fileRanks)))]=ranks[r]
    dfa,ssa,msa,fa,NA,dfb,ssb,msb,fb,NB,dfi,ssi,msi,fi,NI,dfe,sse,mse,dft,sst,NT=twowayAnova(label1,label2,fileRanks,labels)
    #Calculate interactions, factor error calculated on true values
    #Calculate adjusted mean-square with N-1
    #Test-static is calculated as dividing by sum of squares
     #calculate sum of sqaures
    label1Data,label1DataLabels=getLabelledData(label1,files,labels)
    label2Data,label2DataLabels=getLabelledData(label2,files,labels)
    interactionData,interactionLabels=getLabelledData(label2,label1Data,labels)
    meani=0
    for file in interactionData:
        meani+=sum(file)/NI

    mean=0
    for file in files:
        mean+=sum(file)/NT

    ssa=0
    for r in label1Data:
        ssa+=NA*NT*(np.mean(r)-mean)**2
    ssb=0
    for s in label2Data:
        ssb+=NB*NT*(np.mean(s)-mean)**2
    ssi=0
    for r in label1Data:
        for s in label2Data:
            ssi+=NT*(meani-np.mean(r)-np.mean(s)+mean)**2
    sse=0
    for d in files:
        sse+=(np.mean(d)-mean)**2
    sst=ssa+ssb+ssi+sse
    
    #calculate mean squares
    msa=ssa/(NA-1)
    msb=ssb/(NB-1)
    msi=ssi/(NI-1)
    mse=sse/(NT-1)
    
    f=sst/mse
    p = sp.stats.f.cdf(f, dfa, dfb)

    
    return f,p
   
def twowayAnova(label1,label2,data,labels):

    #Seperate data
    label1Data,label1DataLabels=getLabelledData(label1,data,labels)
    label2Data,label2DataLabels=getLabelledData(label2,data,labels)
    interactionData=getLabelledData(label2,label1Data,label1DataLabels)
    NA=len(label1Data)
    NB=len(label2Data)
    NI=len(interactionData)
    NT=len(data)
    meani=0
    for d in data:
        meani+=sum(d)/NI
    mean=0
    for d in data:
        mean+=sum(d)/NT

    #calculate degrees of freedom
    dfa=NA-1
    dfb=NB-1
    dfi=dfa*dfb
    dfe=NA*NB*(NT-1)
    dft=NA*NB*NT-1
    
    #calculate sum of sqaures
    ssa=0
    for r in label1Data:
        ssa+=NA*NT*(np.mean(r)-mean)**2
    ssb=0
    for s in label2Data:
        ssb+=NB*NT*(np.mean(s)-mean)**2
    ssi=0
    for r in label1Data:
        for s in label2Data:
            ssi+=NT*(meani-np.mean(r)-np.mean(s)+mean)**2
    sse=0
    for d in data:
        sse+=(np.mean(d)-mean)**2
    sst=ssa+ssb+ssi+sse
    
    #calculate mean squares
    msa=ssa/(NA-1)
    msb=ssb/(NB-1)
    msi=ssi/(NI-1)
    mse=sse/(NT-1)
    
    #calculate F-Value
    fa=msa/mse
    fb=msb/mse
    fi=msi/mse
    
    return dfa,ssa,msa,fa,NA,dfb,ssb,msb,fb,NB,dfi,ssi,msi,fi,NI,dfe,sse,mse,dft,sst,NT

def getLabelledData(label, data, labels):
    returnData=[]
    returnLabels=[]
    for x in range(len(data)):
        if (label in labels[x]):
            returnData.append(data[x])
            returnLabels.append(labels[x])
    return returnData,returnLabels
    
files=[]
for fileName in glob.glob('*-summary.txt'):
    files+=[dataFromFile(fileName)]

Max=[]
for fileName in glob.glob('*Max-summary.txt'):
    Max+=[dataFromFile(fileName)]

EA=[]
for fileName in glob.glob('*EA-summary.txt'):
    EA+=[dataFromFile(fileName)]

for fileName in glob.glob('*EAMax-summary.txt'):
    EA+=[dataFromFile(fileName)]

GA=[]
for fileName in glob.glob('*GA-summary.txt'):
    GA+=[dataFromFile(fileName)]

for fileName in glob.glob('*GAMax-summary.txt'):
    GA+=[dataFromFile(fileName)]

Repair=[]
for fileName in glob.glob('*Repair-summary.txt'):
    Repair+=[dataFromFile(fileName)]

Random=[]
for fileName in glob.glob('*Random-summary.txt'):
    Random+=[dataFromFile(fileName)]

tenplusten=[]
for fileName in glob.glob('10+10*-summary.txt'):
    tenplusten+=[dataFromFile(fileName)]

oneplusone=[]
for fileName in glob.glob('1+1*-summary.txt'):
    oneplusone+=[dataFromFile(fileName)]

twentyplustwenty=[]
for fileName in glob.glob('20+20*-summary.txt'):
    twentyplustwenty+=[dataFromFile(fileName)]

fiftyplus=[]
for fileName in glob.glob('50+*-summary.txt'):
    fiftyplus+=[dataFromFile(fileName)]

labels=[[] for i in range(len(files))]
for f in range(len(files)):
    if files[f] in Max:
        labels[f].append("Max")
    if files[f] in EA:
        labels[f].append("EA")
    if files[f] in GA:
        labels[f].append("GA")
    if files[f] in Repair:
        labels[f].append("Repair")
    if files[f] in Random:
        labels[f].append("Random")
    if files[f] in oneplusone:
        labels[f].append("1+1")
    if files[f] in tenplusten:
        labels[f].append("10+10")
    if files[f] in twentyplustwenty:
        labels[f].append("20+20")
    if files[f] in fiftyplus:
        labels[f].append("50+")

factors=["Max","EA","GA","Repair","Random","1+1","10+10","50+"]
pvalues=np.zeros((len(factors),len(factors)))
for factor1 in range(len(factors)):
    for factor2 in range(len(factors)):
        if (factor1!=factor2):
            f,p=compareDistribution(factors[factor1],factors[factor2],files,labels)
            pvalues[factor1][factor2]=p
print(pvalues)