# -*- coding: utf-8 -*-
"""
Created on Sun Apr 06 03:22:08 2017

@author: George O'Brien
"""
import scipy.stats as sp
import glob
import matplotlib.pyplot as plt
import numpy as np

def mean(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return (sum(results)/len(results))

def dataFromFile(fileName):
    results=[]
    file = open(fileName,"r")
    file=file.read()
    file=file.split(" ")[5::6]
    for value in file:
        value=value.split(":")[1]
        value=float(value)
        results.append(value)
    return results
    
def compareDistribution(file1,file2):
    x=(file1)
    y=(file2) #legacy code. Don't worry about it
    if(x!=y):
        return sp.kruskal(x,y,alternative="greater")
    else:
        return ("NA",1)
        
def plotCertainty(results):
    notSignificantPairs=[]
    certainty=np.zeros((len(results)+1,len(results)+1))
    for file1 in range(len(results)):
        for file2 in range(len(results)):
            u,p=compareDistribution(results[file1],results[file2])
            certainty[file1+1][file2+1]=p
            if (p<0.01 and file1!=file2):
                print("Comparing",results[file1]," and",results[file2],"gives a u of",u,"and a p of",p)
                print("Therefore there is a significant difference")
                notSignificantPairs.append([results[file1],results[file2]])
    plt.imshow(certainty, cmap='hot', interpolation='nearest')
    return notSignificantPairs


files=[]
for fileName in glob.glob('*-summary.txt'):
    files+=[dataFromFile(fileName)]
certainty=np.zeros((len(files)+1,len(files)+1))

EA=[]
for fileName in glob.glob('*EA-summary.txt'):
    EA+=[dataFromFile(fileName)]

for fileName in glob.glob('*EAMax-summary.txt'):
    EA+=[dataFromFile(fileName)]

GA=[]
for fileName in glob.glob('*GA-summary.txt'):
    GA+=[dataFromFile(fileName)]
    print(fileName)

for fileName in glob.glob('*GAMax-summary.txt'):
    GA+=[dataFromFile(fileName)]
    print(fileName)

Repair=[]
for fileName in glob.glob('*Repair-summary.txt'):
    Repair+=[dataFromFile(fileName)]

P3=[]
for fileName in glob.glob('*P3-summary.txt'):
    P3+=[dataFromFile(fileName)]

#plotCertainty(files)
#print(EA)    
#plotCertainty(EA)
#u,p=sp.kruskal(EA[0],EA[1],EA[2],EA[3],EA[5],EA[6])
plotCertainty(GA)
u,p=sp.kruskal((GA[0]),(GA[1]),(GA[2]),(GA[3]),(GA[4]))
#plotCertainty(Repair)
#u,p=sp.kruskal(Repair[0],Repair[2])
#print(notSignificantPairs)
#u,p=sp.kruskal((files[0]),(files[1]),(files[2]),(files[3]),(files[4]),(files[5]),(files[6]),(files[7]),(files[8]),(files[9]),(files[10]),(files[11]),(files[12]),(files[13]))
#u,p=sp.kruskal(EA[0],EA[1],EA[2],EA[3],EA[4],EA[5],EA[6],(GA[0]),(GA[1]),(GA[2]),(GA[3]),(GA[4]),P3[0])
print((1-p)*100)