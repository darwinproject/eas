# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 14:18:38 2017

@author: Michael
"""

import random
import os
import numpy as np

from statsmodels import robust

from deap import base
from deap import creator
from deap import tools

import ea_util as util

import time
t0 = time.time()

# Process commandline arguments here
problem = 'onemax'
fast = False
MU = 1
LAMBDA = 1
crossover = True
max_evals = 100
BETA = 1.5
f = 'MKP_100_5_0.txt'
test_folder = '../MKP/test_data2/'

creator.create("FitnessMax", base.Fitness, weights=(1.0, ))
creator.create("Individual", np.ndarray, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)
toolbox.register("mate", tools.cxUniform, indpb=0.5)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
toolbox.register("select", tools.selBest)
toolbox.register("selectParents", util.selParents)

random.seed(1000)

# Set the output folder here
results_folder = ('results/' + problem + '/' + str(MU) + '+' + str(LAMBDA) +
                  ('Fast' if fast else '') +
                  ('GA' if crossover else 'EA') + '/')
if not os.path.exists(results_folder):
    os.makedirs(results_folder)


def oneMax():
    evals = []
    failures = 0
    length = int(f.split('_')[2])

    toolbox.register("evaluate", util.evalOneMax)
    toolbox.register("individual", tools.initRepeat, creator.Individual,
                     toolbox.attr_bool, length)
    toolbox.register("population", tools.initRepeat, list,
                     toolbox.individual)

    # Run the algorithm
    pop, log, hof = util.main(length, MU, LAMBDA, toolbox,
                              crossover=crossover, fast=fast)

    with open(results_folder+f, 'w') as l:
            print(log, file=l)
    evals.append(sum(log.select("nevals")))

    if (util.evalOneMax(hof[0]) != 1.0):
        failures += 1

    # Print result
    result = ("Run:" + str(0) +
              " Evals:" + str(evals[-1]) +
              " MES:" + str(np.median(evals)) +
              " MAD:" + str(robust.mad(evals)) +
              " FAILURES:" + str(failures) +
              " Best:" + str(hof[0].fitness.values[0]) +
              " Solution:1.0")
    print(result)


def ising():
    evals = []
    failures = 0

    length = int(f.split('_')[2])

    (min_energy, solution, number_of_spins,
     spins) = util.readIsing(test_folder+f)
    span = number_of_spins - min_energy

    toolbox.register("evaluate", util.evalIsing, spins, min_energy, span)
    toolbox.register("individual", tools.initRepeat, creator.Individual,
                     toolbox.attr_bool, length)
    toolbox.register("population", tools.initRepeat, list,
                     toolbox.individual)

    # Run the algorithm
    pop, log, hof = util.main(length, MU, LAMBDA, toolbox,
                              crossover=crossover, fast=fast)

    with open(results_folder+f, 'w') as l:
            print(log, file=l)
    evals.append(sum(log.select("nevals")))

    if (hof[0].fitness.values[0] < 1):
        failures += 1

    # Print result
    result = ("Run:" + str(0) +
              " Evals:" + str(evals[-1]) +
              " MES:" + str(np.median(evals)) +
              " MAD:" + str(robust.mad(evals)) +
              " FAILURES:" + str(failures) +
              " Best:" + str(hof[0].fitness.values[0]) +
              " Solution:1.0")
    print(result)


def maxSat():
    evals = []
    failures = 0

    length = int(f.split('_')[1])
    solution, signs, clauses = util.readMaxSat(test_folder+f)

    # Structure initializers
    toolbox.register('evaluate', util.evalMaxSat, signs, clauses)
    toolbox.register('individual', tools.initRepeat, creator.Individual,
                     toolbox.attr_bool, length)
    toolbox.register('population', tools.initRepeat, list,
                     toolbox.individual)

    pop, log, hof = util.main(length, MU, LAMBDA, toolbox,
                              crossover=crossover, fast=fast)

    with open(results_folder+f, 'w') as l:
            print(log, file=l)
    evals.append(sum(log.select('nevals')))
    if (hof[0].fitness.values[0] < 1.0):
        failures += 1

    # Print result
    result = ("Run:" + str(0) +
              " Evals:" + str(evals[-1]) +
              " MES:" + str(np.median(evals)) +
              " MAD:" + str(robust.mad(evals)) +
              " FAILURES:" + str(failures) +
              " Best:" + str(hof[0].fitness.values[0]) +
              " Solution:1.0")
    print(result)


def MKP():
    global problems
    (N, M, O, values, coefficients,
     capacities) = util.readMKP(test_folder+f)
    evals = []
    failures = 0

    toolbox.register("evaluate", util.evalKnapsack, O, values, capacities,
                     coefficients)
    toolbox.register("individual", util.generateKnapsack,
                     creator.Individual, N, capacities, coefficients)
    toolbox.register("population", tools.initRepeat, list,
                     toolbox.individual)
    toolbox.register("mutate", tools.mutFlipBit, indpb=1/N)
    pop, log, hof = util.main(N, MU, LAMBDA, toolbox,
                              crossover=crossover, fast=fast)
    evals.append(sum(log.select("nevals")))

    with open(results_folder+f, 'w') as l:
            print(log, file=l)

    if (hof[0].fitness.values[0] < int(O)):
        failures += 1

    result = ("Run:" + str(0) +
              " Evals:" + str(evals[-1]) +
              " MES:" + str(np.median(evals)) +
              " MAD:" + str(robust.mad(evals)) +
              " FAILURES:" + str(failures) +
              " Best:" + str(hof[0].fitness.values[0]) +
              " Solution:1.0")
    print(result)

MKP()

t1 = time.time()
print(t1-t0)
