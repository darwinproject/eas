#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 22 14:37:02 2017

@author: michael
"""

import ea_util as util
import numpy as np

N, M, O, values, coefficients, capacities = util.readMKP('../MKP/test_data2/MKP_100_10_10.txt')
I = range(M)
J = range(N)

def fractionalKnapsack(W, arr):
    n = len(arr)
    weights = np.zeros(n)
    arr.sort(key=lambda a: float(a[0])/float(a[1]), reverse=True)

    curWeight = 0     # Current weight in knapsack
    finalvalue = 0.0  # Result (value in Knapsack)

    # Looping through all Items
    for value, weight, index in arr:
        # If adding Item won't overflow, add it completely
        if (curWeight + weight <= W):
            curWeight += weight
            finalvalue += value
            weights[index] = 1
        # If we can't add current Item, add fractional part of it
        else:
            remain = W - curWeight
            finalvalue += value * (float(remain) / weight)
            weights[index] = float(remain) / weight
            break
    return finalvalue


def repairKnapsack(individual, capacities, coefficients, N, M):
    if util.validKnapsack(individual, capacities, coefficients):
        return individual
    else:
        a = []
        for k, v in enumerate(coefficients):
            tuples = list(zip(values, v, range(len(v))))
            a.append(fractionalKnapsack(capacities[k], tuples))
        ss = [values[j]/sum([a[i]*coefficients[i][j] for i in range(M)]) for j in range(N)]
        ordering = sorted(list(enumerate(ss)), key=lambda x: x[1])
        print([i for i, s in ordering])
        R = [sum([coefficients[i][j] * individual[j] for j in range(N)]) for i in I]
        for j, s in ordering:
            if individual[j] and any([R[i] > capacities[i] for i in I]):
                individual[j] = 0
                R = [R[i] - coefficients[i][j] for i in I]
        for j, s in reversed(ordering):
            if not individual[j] and all([R[i] + coefficients[i][j] <= capacities[i] for i in I]):
                individual[j] = 1
                R = [R[i] + coefficients[i][j] for i in I]
        return individual

def repairKnapsack2(individual, capacities, coefficients, N, M):
    if util.validKnapsack(individual, capacities, coefficients):
        return individual
    else:
        R = [sum([coefficients[i][j] * individual[j] for j in range(N)]) for i in I]
        for j, s in enumerate(individual):
            if individual[j] and any([R[i] > capacities[i] for i in I]):
                individual[j] = 0
                R = [R[i] - coefficients[i][j] for i in I]
        for j, s in enumerate(individual):
            if not individual[j] and all([R[i] + coefficients[i][j] <= capacities[i] for i in I]):
                individual[j] = 1
                R = [R[i] + coefficients[i][j] for i in I]
        return individual


repaired = repairKnapsack(np.ones(N, dtype=int), capacities, coefficients, N, M)
repaired2 = repairKnapsack2(np.ones(N, dtype=int), capacities, coefficients, N, M)

print(util.evalKnapsack(O, values, capacities, coefficients, repaired))
print(util.evalKnapsack(O, values, capacities, coefficients, repaired2))
