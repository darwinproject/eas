N=(100 250 500)
M=(5 10 30)

# Random
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 10000 -l 1 -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done



# (1 + 1) EA
#for m in ${M[@]}
#do
#        for n in ${n[@]}
#        do
#                echo "$m, $n"
#                for seed in {0..29..1}
#                do
#                        (>&2 echo "    $seed")
#                        sum=$((l+s+seed))
#                        python solver.py -p mkp -m 1 -l 1 -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
#        
#                done
#        done
#done

# Greedy (2 + 1) EA
#for m in ${M[@]}
#do
#        for n in ${n[@]}
#        do
#                echo "$m, $n"
#                for seed in {0..29..1}
#                do
#                        (>&2 echo "    $seed")
#                        sum=$((l+s+seed))
#                        python solver.py -p mkp -S uniform -a twoplusone -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
#        
#                done
#        done
#done

# (10 + 1) EA
#for m in ${M[@]}
#do
#        for n in ${n[@]}
#        do
#                echo "$m, $n"
#                for seed in {0..29..1}
#                do
#                        (>&2 echo "    $seed")
#                        sum=$((l+s+seed))
#                        python solver.py -p mkp -m 10 -l 1 -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
#        
#                done
#        done
#done

# (10+1) GA
#for m in ${M[@]}
#do
#        for n in ${n[@]}
#        do
#                echo "$m, $n"
#                for seed in {0..29..1}
#                do
#                        (>&2 echo "    $seed")
#                        sum=$((l+s+seed))
#                        python solver.py -p mkp -m 10 -l 1 -c -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
#        
#                done
#        done
#done

# (20 + 20) EA
mkdir -p summaries/MKP/20+20EA
for l in ${LENGTHS[@]}
do
	echo $l
	(for seed in {0..199..1}
	do
		(>&2 echo "    $seed")
		sum=$((l+seed))
		python solver.py -p MKP -m 20 -l 20 -S uniform -a plus -r $sum -e 10000 MKP_${l}_$seed.txt
	
	done) > summaries/MKP/20+20EA/MKP_${l}_$seed.txt
done

# (20 + 20) GA
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 20 -l 20 -c -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done

# (100 + 1) EA
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 100 -l 1 -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done

# (1 + 1) Fast EA
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 1 -l 1 -f -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done

# (20 + 20) Fast GA
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 20 -l 20 -f -c -S uniform -a plus -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done

# (1 + (8, 8)) EA
for m in ${M[@]}
do
        for n in ${n[@]}
        do
                echo "$m, $n"
                for seed in {0..29..1}
                do
                        (>&2 echo "    $seed")
                        sum=$((l+s+seed))
                        python solver.py -p mkp -m 1 -l 8 -S uniform -a lambdalambda -r $sum -e 10000 test_data/MKP/MKP_${l}_${s}_$seed.txt
        
                done
        done
done


