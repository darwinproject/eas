#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 22 11:27:30 2017

@author: michael
"""
import ea_util as util
import numpy as np

N, M, O, values, r, b = util.readMKP('../MKP/mknapsmall.txt')

I = range(M)
R = [sum([r[i][j] for j in range(N)]) for i in I]

x = np.ones(N, dtype=int)
print(x)

for j in reversed(range(N)):
    if (x[j] == 1) and any([R[i] > b[i] for i in I]):
        x[j] = 0
        for i in I:
            R[i] -= r[i][j]
