import numpy as np
import ea_util as util
import math


def validKnapsack(individual, capacities, coefficients):
    constraints = np.dot(coefficients, individual)
    return all([i < 0 for i in(constraints - capacities)])


def fractionalKnapsack(W, arr):
    n = len(arr)
    weights = np.zeros(n)
    arr.sort(key=lambda a: float(a[0])/float(a[1]), reverse=True)

    curWeight = 0     # Current weight in knapsack
    finalvalue = 0.0  # Result (value in Knapsack)

    # Looping through all Items
    for value, weight, index in arr:
        # If adding Item won't overflow, add it completely
        if (curWeight + weight <= W):
            curWeight += weight
            finalvalue += value
            weights[index] = 1
        # If we can't add current Item, add fractional part of it
        else:
            remain = W - curWeight
            finalvalue += value * (float(remain) / weight)
            weights[index] = float(remain) / weight
            break
    return finalvalue, weights

# Read in the problem
N, M, O, values, coefficients, capacities = util.readMKP('../MKP/mknapsmall.txt')

# Step one
zBar = math.inf
iStar = 0
xBar = None
# For each constraint vector, solve the continuous knapsack problem
for k, v in enumerate(coefficients):
    tuples = list(zip(values, v, range(len(v))))
    value, weights = fractionalKnapsack(capacities[k], tuples)
    if value < zBar:
        zBar = value
        iStar = k
        xBar = weights
# print(iStar, zBar, xBar)

print(xBar)
print()

# Step two - find out how much we validate
constraints = np.dot(coefficients, xBar)
violation = constraints - capacities
print(violation)

while any([i > 0 for i in (constraints - capacities)]):
    iPrime = violation.tolist().index(max(violation))
    print(iPrime)
    cOne = iStar
    cTwo = iPrime
    break
